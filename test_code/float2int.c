#include<stdio.h>
#include<stdint.h>
#include<string.h>

#define COPY_TO_U32 0

/*
 * float -> u32 -> float
 */
void main(void)
{
    uint16_t    register_tabs[2];
    uint32_t    register_copy;

    float       register_value = 27.26;
    uint32_t    register_read;

    int a, b, c;

    a = (b = (c = 1));

    printf("%d\r\n", a);
#if COPY_TO_U32
    memcpy(&register_copy, &register_value, sizeof(uint32_t));
    register_tabs[0] = (uint16_t)register_copy;
    register_tabs[1] = (uint16_t)(register_copy>>16);
#else
    //register_tabs[0] = (uint16_t)(register_value);
    //register_tabs[1] = (uint16_t)((uint32_t)register_value>>16);
    register_tabs[0] = (uint16_t)(*(uint32_t*)&register_value);
    register_tabs[1] = (uint16_t)(*(uint32_t*)&register_value>>16);
#endif

    printf("=========> COPY_TO_U32 [%s]\r\n", COPY_TO_U32?"enable":"disable");
    printf("HEX:    register_value[0x%08X] register_tabs[0x%08X]\r\n", *((uint32_t*)&register_value), *((uint32_t*)&register_tabs));
    printf("FLOAT:  register_value[%010f] register_tabs[%010f]\r\n", (float)register_value, *((float*)&register_tabs));

    register_read = register_tabs[0] + (register_tabs[1]<<16);

    printf("HEX:  register_tabs[0x%X] register_read[0x%X]\r\n", *((uint32_t*)&register_tabs), register_read);
    printf("%f %f\r\n", (float)register_read, *((float*)&register_read));
}
