#include <stdio.h>
#include <stdlib.h>

void main(void)
{
	char **p = malloc(sizeof(char)*1024);
	char **t;

	int i = 0;

	memset(p,0xFF,1024);

	for(t=p; *t; t++)
	{
		printf("%d\r\n", i++);
	}

	free(p);
}
