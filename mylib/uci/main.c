/************************************************************************************************
 ** @File:    main.c
 ** @Author:  JanChiang
 ** @Date:    2024/01/26
 ** @Brief:   TThis file provides the API for main
 ************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "uci_sources/uci.h"
/* Public variables --------------------------------------------------------*/
/* Public macros -----------------------------------------------------------*/
/* Public typedef ----------------------------------------------------------*/
/* Public functions --------------------------------------------------------*/
/* Private variables -------------------------------------------------------*/
/* Private macros ----------------------------------------------------------*/
/* Private typedef ---------------------------------------------------------*/
/* Private functions -------------------------------------------------------*/
static uci_void uci_parseParameters(uci_int32 argc, uci_char *argv[], uci_parameter_t *uci_parms)
{
    uci_int32   cmd_type = 0;
    uci_int32   semicolon_token_index;
    uci_char    *semicolon_token;
    uci_char    *end_str_ptr;
    uci_char    *val_str_ptr;

    if(argc >= 2)
    {
        LOG_DEBUG("command: %s", argv[1]);

        for(cmd_type = 0; cmd_type < UCI_COMMAND_MAX; cmd_type++)
        {
            if(strcasecmp(argv[1], uci_cmd_list[cmd_type].match_string) == 0)
                break;
        }
        
        uci_parms->command_type = cmd_type<UCI_COMMAND_MAX?cmd_type:UCI_COMMAND_HELP;
    }


    if(argc == 3)
    {
        if((val_str_ptr = strchr(argv[2], '=')))
        {
            val_str_ptr[0] = 0;
            val_str_ptr++;
            LOG_DEBUG("value: %s", val_str_ptr);
            uci_parms->set_value = uci_malloc_string(val_str_ptr);
        }

        semicolon_token = strtok_r(argv[2], "." , &end_str_ptr);
        semicolon_token_index = UCI_CMDLINE_CFG_FILE;

        while (semicolon_token != NULL)
        {
            LOG_DEBUG("taken %d => %s", semicolon_token_index-UCI_CMDLINE_CFG_FILE, semicolon_token);

            switch (semicolon_token_index)
            {
            case UCI_CMDLINE_CFG_FILE:
                uci_parms->config = uci_malloc_string(semicolon_token);
                uci_parms->command_target = UCI_CMDLINE_CFG_FILE;
                break;
            case UCI_CMDLINE_SECTION:
                uci_parms->section = uci_malloc_string(semicolon_token);
                uci_parms->command_target = UCI_CMDLINE_SECTION;
                break;
            case UCI_CMDLINE_OPTION:
                uci_parms->option = uci_malloc_string(semicolon_token);
                uci_parms->command_target = UCI_CMDLINE_OPTION;
                break;
            default:
                break;
            }
            semicolon_token = strtok_r(NULL, "." , &end_str_ptr);
            semicolon_token_index++;
        }
    }
    else if(argc == 4 && uci_parms->command_type == UCI_COMMAND_ADD)
    {
        uci_parms->config =  uci_malloc_string(argv[2]);
        /* 取消匿名节点,TODO */
        // uci_parms->section_type = XXX;
    }
}


uci_int32 main(uci_int32 argc, uci_char *argv[])
{
    uci_parameter_t uci_parms;

    /* 初始化内存探针表 */
    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);

    uci_initParameters(&uci_parms);
    uci_parseParameters(argc, argv, &uci_parms);

    /* 执行用户命令 see@uci_cmd_list */
    uci_runCommand(&uci_parms);

    uci_freeParameters(&uci_parms);

    /* 当下面的函数生效时，说明有未释放的指针，请使能UCI_DEBUG_ENABLED检查。 */
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);
    
    uci_parms.error_code = uci_getErrorCode();

    return uci_parms.error_code;
}