/************************************************************************************************
 ** @File:    test.c
 ** @Author:  JanChiang
 ** @Date:    2024/01/29
 ** @Brief:   TThis file provides the API for test
 ************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "uci_sources/uci.h"
/* Public variables --------------------------------------------------------*/
/* Public macros -----------------------------------------------------------*/
/* Public typedef ----------------------------------------------------------*/
/* Public functions --------------------------------------------------------*/
/* Private variables -------------------------------------------------------*/
/* Private macros ----------------------------------------------------------*/
/* Private typedef ---------------------------------------------------------*/
/* Private functions -------------------------------------------------------*/
uci_errno_t error_code;

#define UCI_TEST_ERR_ASSERT(exp)            do {  \
                                            if((error_code = (exp))) \
                                                {    \
                                                    LOG_PRINT("[Test Code Line: %3d] => Error code: %d", __LINE__, error_code);    \
                                                    goto err;\
                                                }  \
                                            else    \
                                                LOG_PRINT("[Test Code Line: %3d] => Done.", __LINE__); \
                                            }while(0)

uci_errno_t main(void)
{
    uci_char read[1024] = {0};

    UCI_TEST_ERR_ASSERT(uci_add_config("test1"));
    UCI_TEST_ERR_ASSERT(uci_set_section("test1", "sec1", "typeTest"));
    UCI_TEST_ERR_ASSERT(uci_set_section("test1", "sec2", "typeTest"));
    UCI_TEST_ERR_ASSERT(uci_set_section("test1", "sec3", "typeTest"));

    UCI_TEST_ERR_ASSERT(uci_set_option("test1", "sec1", "opt1", "AAA"));
    UCI_TEST_ERR_ASSERT(uci_set_option("test1", "sec2", "opt1", "AAA"));
    UCI_TEST_ERR_ASSERT(uci_set_option("test1", "sec3", "opt1", "AAA"));

    UCI_TEST_ERR_ASSERT(uci_commit("test1"));

    UCI_TEST_ERR_ASSERT(uci_get_section("test1", "sec1", read, 1024));
    LOG_PRINT("[Test Code Line: %3d] => %s", __LINE__, read);
    UCI_TEST_ERR_ASSERT(uci_get_section("test1", "sec2", read, 1024));
    LOG_PRINT("[Test Code Line: %3d] => %s", __LINE__, read);
    UCI_TEST_ERR_ASSERT(uci_get_section("test1", "sec3", read, 1024));
    LOG_PRINT("[Test Code Line: %3d] => %s", __LINE__, read);

    UCI_TEST_ERR_ASSERT(uci_get_option("test1", "sec1", "opt1", read, 1024));
    LOG_PRINT("[Test Code Line: %3d] => %s", __LINE__, read);
    UCI_TEST_ERR_ASSERT(uci_get_option("test1", "sec2", "opt1", read, 1024));
    LOG_PRINT("[Test Code Line: %3d] => %s", __LINE__, read);
    UCI_TEST_ERR_ASSERT(uci_get_option("test1", "sec3", "opt1", read, 1024));
    LOG_PRINT("[Test Code Line: %3d] => %s", __LINE__, read);

    UCI_TEST_ERR_ASSERT(uci_del_option("test1", "sec1", "opt1"));
    UCI_TEST_ERR_ASSERT(uci_del_section("test1", "sec2"));
    UCI_TEST_ERR_ASSERT(uci_rename_section("test1", "sec3", "secRenamed"));
    UCI_TEST_ERR_ASSERT(uci_commit("test1"));
    UCI_TEST_ERR_ASSERT(uci_rename_option("test1", "secRenamed", "opt1", "optRenamed"));
    UCI_TEST_ERR_ASSERT(uci_commit("test1"));

    UCI_TEST_ERR_ASSERT(uci_add_config("test2"));
    UCI_TEST_ERR_ASSERT(uci_set_section("test2", "sec1", "typeTest"));
    UCI_TEST_ERR_ASSERT(uci_get_section("test2", "sec1", read, 1024));
    LOG_PRINT("[Test Code Line: %3d] => %s", __LINE__, read);
    UCI_TEST_ERR_ASSERT(uci_del_config("test2"));
    UCI_TEST_ERR_ASSERT(uci_commit("test2"));

err:
    return error_code;
}