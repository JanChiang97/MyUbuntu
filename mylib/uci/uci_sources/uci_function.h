/************************************************************************************************
 ** @File:    uci_function.h
 ** @Author:  Jan
 ** @Date:    2024/01/04
 ** @Brief:   This file include uci_function.c struct and manipulation functions.
 ************************************************************************************************/

#ifndef UCI_FUNCTION_H
#define UCI_FUNCTION_H
#ifdef __cplusplus ///<use C compiler                                           
extern "C"
{
#endif //__cplusplus
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>
#include "uci.h"

/* Exported variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
typedef enum
{
    UCI_COMMAND_HELP,
    UCI_COMMAND_SHOW,
    UCI_COMMAND_SET,
    UCI_COMMAND_GET,
    UCI_COMMAND_ADD,
    UCI_COMMAND_ADD_LIST,
    UCI_COMMAND_DEL,
    UCI_COMMAND_DEL_LIST,
    UCI_COMMAND_RENAME,
    UCI_COMMAND_CHANGES,
    UCI_COMMAND_COMMIT,
    UCI_COMMAND_TEST,
    UCI_COMMAND_MAX,
} uci_command_type_t;

typedef struct uci_function
{
    uci_command_type_t  type;
    uci_char            match_string[16];
    uci_char            description[UCI_CMD_DECRIPTION_LENGTH];
    uci_void            (*callback)(uci_parameter_t *uci_parms);
} uci_cmd_list_t;

/* Exported functions --------------------------------------------------------*/
uci_void uci_runCommand(uci_parameter_t *uci_parms);

extern uci_void uci_runHelpCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runShowCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runSetCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runGetCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runAddCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runAddListCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runDelCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runDelListCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runRenameCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runChangesCommand(uci_parameter_t *uci_parms);
extern uci_void uci_runCommitCommand(uci_parameter_t *uci_parms);

/* Insert function API at here */
static uci_cmd_list_t uci_cmd_list[UCI_COMMAND_MAX] = 
{
    /* COMMAND_ID               COMMAND     DESCRIPTION                                 CALLBACK */
    {UCI_COMMAND_HELP,          "help",     "show this message.",                       &uci_runHelpCommand},
    {UCI_COMMAND_SHOW,          "show",     "[<config>[.<section>[.<option>]]]",        &uci_runShowCommand},
    {UCI_COMMAND_SET,           "set",      "<config>.<section>[.<option>]=<value>",    &uci_runSetCommand},
    {UCI_COMMAND_GET,           "get",      "<config>.<section>[.<option>]",            &uci_runGetCommand},
    /* 取消匿名节点的设定 */
    // {UCI_COMMAND_ADD,           "add",      "<config> <section-type>",                  &uci_runAddCommand}, 
    {UCI_COMMAND_ADD,           "add",      "<config>",    								&uci_runAddCommand},
    {UCI_COMMAND_ADD_LIST,      "add_list", "<config>.<section>.<option>=<string>",     &uci_runAddListCommand},
    {UCI_COMMAND_DEL,           "del",      "<config>[.<section>[.<option>]]",          &uci_runDelCommand},
    {UCI_COMMAND_DEL_LIST,      "del_list", "<config>.<section>.<option>=<string>",     &uci_runDelListCommand},
    {UCI_COMMAND_RENAME,        "rename",   "<config>.<section>[.<option>]=<name>",     &uci_runRenameCommand},
    {UCI_COMMAND_CHANGES,       "changes",  "[<config>]",                               &uci_runChangesCommand},
    {UCI_COMMAND_COMMIT,        "commit",   "[<config>]",                               &uci_runCommitCommand},
};

#ifdef __cplusplus ///<use C compiler
}
#endif //__cplusplus
#endif
