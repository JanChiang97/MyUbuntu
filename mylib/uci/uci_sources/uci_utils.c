/************************************************************************************************
 ** @File:    uci_utils.c
 ** @Author:  JanChiang
 ** @Date:    2024/01/04
 ** @Brief:   TThis file provides the API for uci_utils
 ************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "uci_utils.h"
/* Public variables --------------------------------------------------------*/
/* Public macros -----------------------------------------------------------*/
/* Public typedef ----------------------------------------------------------*/
/* Private variables -------------------------------------------------------*/
/* Private macros ----------------------------------------------------------*/
/* Private typedef ---------------------------------------------------------*/
/* Private functions -------------------------------------------------------*/
static uci_bool uci_validateString(const uci_char *str, uci_bool name)
{
	if (!*str)
		return false;

	while (*str) 
    {
		unsigned char c = *str;
		if (!isalnum(c) && c != '_') 
        {
			if (name || (c < 32) || (c > 126))
            {
                UCI_DEBUG_ERROR(UCI_ERROR_CFG_FORMAT);
				return false;
            }
		}
		str++;
	}

	return true;
}

static uci_char *uci_getListValue(uci_char *source)
{
    uci_char *h;
    uci_char *end;
    uci_int len;

    UCI_PARMS_ASSERT(source);

    if(source[0] != '"' && source[0] != '\'') UCI_DEBUG_ERROR(UCI_ERROR_CFG_FORMAT);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_NULL);

    h = source;

    /* skip quote character */
    source+=1;

    end = strchr(source, h[0]);
    if(!end) UCI_DEBUG_ERROR(UCI_ERROR_CFG_FORMAT);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_NULL);

    end[1] = 0;

    return h;
}


static char *get_filename(char *path)
{
	char *p;

	p = strrchr(path, '/');
	p++;
	if (!*p)
		return NULL;
	return p;
}

static uci_char *uci_deleteQuotes(const uci_char *src, uci_char *dst)
{
    uci_char h = 0;
    uci_char *p;

    UCI_PARMS_ASSERT(src && dst);

    if(src[0] != '"' && src[0] != '\'') 
    {
        strcpy(dst, src);
        return dst;
    }

    h = src[0];
    p = dst;

    while (++src)
    {
        if(*src == 0) {UCI_DEBUG_ERROR(UCI_ERROR_CFG_FORMAT);UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_NULL);}
        if(*src == h) {*p=0;break;}
        *p = *src;
        p++;
    }
    
    return dst;
}


static uci_void uci_deleteFile(const uci_char *name, const uci_char *file_dir)
{
    uci_char *filename;

    UCI_PARMS_ASSERT(name && file_dir);

    LOG_DEBUG("Clean: %s%s",file_dir, name);
    filename = uci_malloc_memory(strlen(name) + strlen(file_dir)+1);
    if(filename)
    {
        sprintf(filename, "%s%s", file_dir, name);
        remove(filename);
        uci_free_memory(filename);
    }
}


/* Public functions --------------------------------------------------------*/
uci_void uci_initParameters(uci_parameter_t *uci_parms)
{
    if(uci_parms)
    {
        uci_parms->command_type = 0;
        uci_parms->command_target = 0;
        uci_parms->error_code   = 0;
        uci_parms->config       = NULL;
        uci_parms->section      = NULL;
        uci_parms->ret_value    = NULL;
        uci_parms->option       = NULL;
        uci_parms->set_value    = NULL;
        uci_parms->section_type = NULL;
    }
}

uci_void uci_freeParameters(uci_parameter_t *uci_parms)
{
    if(uci_parms)
    {
        if(uci_parms->config)       uci_free_memory(uci_parms->config);
        if(uci_parms->section_type) uci_free_memory(uci_parms->section_type);
        if(uci_parms->section)      uci_free_memory(uci_parms->section);
        if(uci_parms->option)       uci_free_memory(uci_parms->option);
        if(uci_parms->set_value)    uci_free_memory(uci_parms->set_value);
        if(uci_parms->ret_value)    uci_free_memory(uci_parms->ret_value);

        uci_parms->config       = NULL;
        uci_parms->section      = NULL;
        uci_parms->ret_value    = NULL;
        uci_parms->option       = NULL;
        uci_parms->set_value    = NULL;
        uci_parms->section_type = NULL;
        /* Did not clear the error_code */
        // uci_parms->error_code;
    }
}

uci_list_t *uci_new_list(uci_void)
{
    uci_list_t *list;
    uci_int len;

    list = uci_malloc_memory(sizeof(uci_list_t));
    list->conf_path = uci_malloc_memory(strlen(UCI_CONFDIR)+2);
    list->save_path = uci_malloc_memory(strlen(UCI_SAVEDIR)+2);

    memcpy(list->conf_path, UCI_CONFDIR, strlen(UCI_CONFDIR));
    memcpy(list->save_path, UCI_SAVEDIR, strlen(UCI_SAVEDIR));

    /* format path */
    len = strlen(list->conf_path);
    if(list->conf_path[len-1] != '/')
        list->conf_path[len] = '/';


    len = strlen(list->save_path);
    if(list->save_path[len-1] != '/')
        list->save_path[len] = '/';

    LOG_DEBUG("conf_path: %s", list->conf_path);
    LOG_DEBUG("save_path: %s", list->save_path);

    mkdir(list->conf_path, UCI_DIRMODE);
    mkdir(list->save_path, UCI_DIRMODE);

    uci_initList(list);

    return list;
}

uci_package_t *uci_new_package(const uci_char *name)
{
    uci_package_t *pkg;

    UCI_PARMS_ASSERT(name);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_NULL);

    pkg = uci_malloc_memory(sizeof(uci_package_t));
    pkg->name = uci_malloc_string(name);

    uci_initPackage(pkg);

    return pkg;
}

uci_section_t *uci_new_section(const uci_char *name, uci_char *type)
{
    uci_section_t *sec;

    UCI_PARMS_ASSERT(type);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_NULL);

    sec = uci_malloc_memory(sizeof(uci_section_t));
    if(name) sec->name = uci_malloc_string(name);
    uci_deleteQuotes(type, type);
    sec->type = uci_malloc_string(type);

    uci_initSection(sec);

    return sec;
}

uci_delta_t *uci_new_delta(const uci_char *name, uci_char *value, uci_modify_cmd_t cmd)
{
    uci_delta_t *delta;

    UCI_PARMS_ASSERT(cmd);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_NULL);

    delta = uci_malloc_memory(sizeof(uci_delta_t));
    
    if(value) 
    {
        uci_deleteQuotes(value, value);
        delta->value = uci_malloc_string(value);
    }

    if(name) delta->name = uci_malloc_string(name);
    /* �����ڵ� */
    else
    {
        ;
    }

    delta->cmd = cmd;

    return delta;
}

uci_element_t *uci_new_element(const uci_char *name, uci_char *value)
{
    uci_element_t *elm;
    UCI_PARMS_ASSERT(name && value);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_NULL);

    elm = uci_malloc_memory(sizeof(uci_element_t));
    elm->name = uci_malloc_string(name);
    uci_deleteQuotes(value, value);
    elm->value = uci_malloc_string(value);

    uci_initElement(elm);

    return elm;
}


uci_void uci_initElement(uci_element_t *element)
{
    if(element)
    {
        UCI_LIST_INIT(element);
        UCI_LIST_INIT(&element->delta);
    }
}

uci_void uci_initSection(uci_section_t *section)
{
    if(section)
    {
        UCI_LIST_INIT(section);
        UCI_LIST_INIT(&section->delta);
        uci_initElement(&section->element);
    }
}

uci_void uci_initPackage(uci_package_t *package)
{
    if(package)
    {
        UCI_LIST_INIT(package);
        UCI_LIST_INIT(&package->delta);
        uci_initSection(&package->section);
    }
}

uci_void uci_initList(uci_list_t *uci_list)
{
    if(uci_list)
    {
        UCI_LIST_INIT(&uci_list->delta);
        uci_initPackage(&uci_list->package);
    }
}

uci_void uci_initDelta(uci_delta_t *delta)
{
    if(delta)
    {
        UCI_LIST_INIT(delta);
    }
}

uci_bool uci_parseLineSection(const uci_char *buf, uci_package_t *uci_package)
{
    uci_char field_secName[UCI_FIELD_LENGTH] = {0};
    uci_char field_secType[UCI_FIELD_LENGTH] = {0};
    uci_section_t *uci_sec;

    UCI_PARMS_ASSERT(buf && uci_package);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_FALSE);

    if(sscanf(buf, "%s %s", field_secType, field_secName)!=EOF)
    {
        if(!uci_deleteQuotes(field_secName, field_secName)) return false;
        LOG_DEBUG("field_secType: %s", field_secType);
        LOG_DEBUG("field_secName: %s", field_secName);
        if(uci_validateString(field_secType, false) && uci_validateString(field_secName, true))
        {
            uci_sec = uci_new_section(field_secName, field_secType);
            uci_insertPackage(uci_package, uci_sec);
            return true;
        }
    }
    
    return false;
}

uci_bool uci_parseLineOption(const uci_char *buf, uci_section_t *uci_section)
{
    uci_char field_optValue[UCI_FIELD_LENGTH] = {0};
    uci_char *pfield_optValue;
    uci_char field_optName[UCI_FIELD_LENGTH] = {0};
    uci_element_t *uci_element;

    UCI_PARMS_ASSERT(buf && uci_section);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_FALSE);

    if(sscanf(buf, "%s %s", field_optName, field_optValue)!=EOF)
    {
        pfield_optValue = strstr(buf+strlen(field_optName), field_optValue);
        LOG_DEBUG("field_optName: %s", field_optName);
        LOG_DEBUG("field_optValue: %s", pfield_optValue);
        if(!uci_deleteQuotes(pfield_optValue, pfield_optValue)) return false;
        LOG_DEBUG("field_optName: %s", field_optName);
        LOG_DEBUG("field_optValue: %s", pfield_optValue);
        if(uci_validateString(pfield_optValue, false) && uci_validateString(field_optName, true))
        {
            uci_element = uci_new_element(field_optName, pfield_optValue);
            uci_element->is_list = false;
            uci_insertSection(uci_section, uci_element);
            return true;
        }
    }
    
    return false;
}


uci_bool uci_parseLineList(const uci_char *buf, uci_section_t *uci_section)
{
    uci_char field_listValue[UCI_FIELD_LENGTH] = {0};
    uci_char field_listName[UCI_FIELD_LENGTH] = {0};
    uci_char *list_str;
    uci_char *p;
    uci_element_t *uci_element;

    UCI_PARMS_ASSERT(buf && uci_section);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_FALSE);

    if(sscanf(buf, "%s %s", field_listName, field_listValue)!=EOF)
    {
        list_str = strstr(buf, field_listValue);
        list_str = uci_getListValue(list_str);

        if(!uci_deleteQuotes(list_str, list_str)) return false;
        LOG_DEBUG("field_listName: %s", field_listName);
        LOG_DEBUG("field_listValue: %s", list_str);
        if(uci_validateString(field_listValue, false) && uci_validateString(field_listName, true))
        {
            uci_element = uci_new_element(field_listName, list_str);
            uci_element->is_list = true;
            uci_insertSection(uci_section, uci_element);
            return true;
        }
    }
    
    return false;
}



uci_void uci_insertSection(uci_section_t *uci_sec, uci_element_t *uci_elm)
{
    UCI_PARMS_ASSERT(uci_sec && uci_elm);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    uci_elm->next = uci_sec->element.next;
    uci_sec->element.next = uci_elm;

    uci_elm->next->prev = uci_elm;
    uci_elm->prev = &uci_sec->element;
    uci_elm->father = (void *)uci_sec;
}

uci_void uci_insertPackage(uci_package_t *uci_pkg, uci_section_t *uci_sec)
{
    UCI_PARMS_ASSERT(uci_pkg && uci_sec);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    uci_sec->next = uci_pkg->section.next;
    uci_pkg->section.next = uci_sec;

    uci_sec->next->prev = uci_sec;
    uci_sec->prev = &uci_pkg->section;
    uci_sec->father = (void *)uci_pkg;
}


uci_void uci_insertList(uci_list_t *uci_list, uci_package_t *uci_pkg)
{
    
    UCI_PARMS_ASSERT(uci_list && uci_pkg);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    uci_pkg->next = uci_list->package.next;
    uci_list->package.next = uci_pkg;

    uci_pkg->next->prev = uci_pkg;
    uci_pkg->prev = &uci_list->package;
    uci_pkg->father = (void *)uci_list;
}

uci_void uci_addDelta(uci_void *father, uci_delta_t *new, uci_delta_type_t type)
{
    uci_delta_t *head;
    uci_delta_t *last;

    UCI_PARMS_ASSERT(father && new);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    if(type == UCI_DELTA_LIST)         head = &((uci_list_t*)father)->delta;
    else if(type == UCI_DELTA_PACKAGE) head = &((uci_package_t*)father)->delta;
    else if(type == UCI_DELTA_SECTION) head = &((uci_section_t*)father)->delta;
    else if(type == UCI_DELTA_ELEMENT) head = &((uci_element_t*)father)->delta;
    else UCI_DEBUG_ERROR(UCI_ERROR_ASSERT);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    new->father = father;
    new->type = type;
    /* ��β����� */
    last = head->prev;

    new->next = head;
    new->prev = last;
    last->next = new;
    head->prev = new;
}

uci_void uci_release_delta(uci_delta_t *delta)
{
    UCI_PARMS_ASSERT(delta);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    delta->cmd = 0;
    delta->type = 0;
    delta->father = NULL;
    if(delta->name) uci_free_memory(delta->name);
    if(delta->value) uci_free_memory(delta->value);

    UCI_LIST_REMOVE_NODE(delta);
    uci_free_memory(delta);
}

uci_void uci_release_element(uci_element_t *uci_element)
{
    uci_delta_t *delta;

	UCI_PARMS_ASSERT(uci_element)
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);


    UCI_LIST_FOREACH(&uci_element->delta, delta)
        if(delta) uci_release_delta(delta);

	if(uci_element->name) uci_free_memory(uci_element->name);
	if(uci_element->value) uci_free_memory(uci_element->value);

    UCI_LIST_REMOVE_NODE(uci_element);
	uci_free_memory(uci_element);
}

uci_void uci_release_section(uci_section_t *uci_section)
{
    uci_delta_t *delta;
    uci_element_t *elm;

	UCI_PARMS_ASSERT(uci_section)
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_section->element, elm)
        if(elm) uci_release_element(elm);

    UCI_LIST_FOREACH(&uci_section->delta, delta)
        if(delta) uci_release_delta(delta);

	if(uci_section->name) uci_free_memory(uci_section->name);
	if(uci_section->type) uci_free_memory(uci_section->type);

    UCI_LIST_REMOVE_NODE(uci_section);
	uci_free_memory(uci_section);
}

uci_void uci_release_package(uci_package_t *uci_package)
{
    uci_delta_t *delta;
    uci_section_t *sec;

	UCI_PARMS_ASSERT(uci_package)
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_package->section, sec)
        if(sec) uci_release_section(sec);

    UCI_LIST_FOREACH(&uci_package->delta, delta)
        if(delta) uci_release_delta(delta);

	if(uci_package->name) uci_free_memory(uci_package->name);

    UCI_LIST_REMOVE_NODE(uci_package);
	uci_free_memory(uci_package);
}

uci_void uci_release_list(uci_list_t *uci_list)
{
    uci_package_t *pkg;
    uci_delta_t *delta;

    UCI_PARMS_ASSERT(uci_list);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_list->package, pkg)
        if(pkg) uci_release_package(pkg);

    UCI_LIST_FOREACH(&uci_list->delta, delta)
        if(delta) uci_release_delta(delta);

    if(uci_list->conf_path) uci_free_memory(uci_list->conf_path);
    if(uci_list->save_path) uci_free_memory(uci_list->save_path);

    uci_free_memory(uci_list);
}


uci_void uci_parseConfigFile(uci_list_t *uci_list, const uci_char *file_name)
{
    uci_char *line_buff;
    uci_char *header, *payload;
    uci_int line_buff_size = UCI_BUFFER_SIZE;
    uci_package_t *pkg;
    uci_file *file;

    UCI_PARMS_ASSERT(uci_list && file_name);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    file = uci_openFileStream(file_name, uci_list->conf_path, SEEK_SET, false, false);
    if(!file) UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    line_buff = uci_malloc_memory(line_buff_size);
    pkg = uci_new_package(file_name);

    do
    {
        header = fgets(line_buff, line_buff_size, file);
        if(!header) break;

        payload = NULL;
        header = strtok_r(line_buff, " \t\r\n", &payload);
        if(!header) continue;
        
        switch (header[0])
        {
        case 0:         /* empty line */
        case '#':       /* Comments */
            break;

        case 'p':       /* package line */
            if(!strcmp(header, "package")) break;
        case 'c':
            if(!strcmp(header, UCI_SECTION_LINE_HEADER))
            {
                if(!uci_parseLineSection(payload, pkg)) 
                    UCI_DEBUG_ERROR(UCI_ERROR_FILE_PARSE);
                UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
            }
            break;
        case 'o':
            if(!strcmp(header, UCI_OPTION_LINE_HEADER))
            {
                if(!uci_parseLineOption(payload, pkg->section.next)) 
                    UCI_DEBUG_ERROR(UCI_ERROR_FILE_PARSE);
                UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
            }
            break;
        case 'l':
            if(!strcmp(header, UCI_LIST_LINE_HEADER))
            {
                if(!uci_parseLineList(payload, pkg->section.next)) 
                    UCI_DEBUG_ERROR(UCI_ERROR_FILE_PARSE);
                UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
            }
            break;
        default:
            UCI_DEBUG_ERROR(UCI_ERROR_CFG_FORMAT);
            UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
            break;
        }

    } while (1);

    uci_closeFileStream(file);
    uci_insertList(uci_list, pkg);
    uci_free_memory(line_buff);
}


uci_void uci_parseConfigDir(uci_list_t *uci_list)
{
    uci_char *dir;
    uci_char *file_name;
    uci_int size, idx;
	glob_t globbuf;

    UCI_PARMS_ASSERT(uci_list && uci_list->conf_path);

    dir = uci_malloc_memory(strlen(uci_list->conf_path)+3);

    if(uci_list->conf_path[strlen(uci_list->conf_path)-1] == '/')
        sprintf(dir, "%s*", uci_list->conf_path);
    else
        sprintf(dir, "%s/*", uci_list->conf_path);

    if(glob(dir, GLOB_MARK, NULL, &globbuf))
    {
        uci_free_memory(dir);
        UCI_DEBUG_ERROR(UCI_ERROR_DIR_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
    }

    for(idx = 0; idx < globbuf.gl_pathc; idx++)
    {
        file_name = get_filename(globbuf.gl_pathv[idx]);
        if(!file_name) continue;
        if(!uci_validateString(file_name, true)) continue;
        uci_parseConfigFile(uci_list, file_name);
    }
    
    uci_free_memory(dir);
    globfree(&globbuf);
}

static uci_void uci_saveDeltaNode(uci_delta_t *delta, uci_file *file)
{
    uci_char *path_cfg = NULL;
    uci_char *path_sec = NULL;
    uci_char *path_elm = NULL;
    uci_char *prefix = "";
    uci_bool sublevel = false;

    UCI_PARMS_ASSERT(delta && file && delta->father && delta->cmd);

    /* record delta node to console */
    LOG_DEBUG("==============================");
    if(delta->cmd)      LOG_DEBUG("COMMAND TYPE: %s", uci_modify_cmd[delta->cmd]);
    if(delta->name)     LOG_DEBUG("TARGET NAME: %s", delta->name);
    if(delta->value)    LOG_DEBUG("TARGET VALUE: %s", delta->value);
    LOG_DEBUG("==============================");

    if(delta->cmd == UCI_MOD_CMD_ADD || delta->cmd == UCI_MOD_CMD_ADD_LIST || delta->cmd == UCI_MOD_CMD_REMOVE) sublevel = true;

    switch (delta->type)
    {
    case UCI_DELTA_LIST:
        path_cfg = delta->name;
        break;
    case UCI_DELTA_PACKAGE:
        path_cfg = ((uci_package_t *)(delta->father))->name;
        if(sublevel) path_sec = delta->name;
        break;
    case UCI_DELTA_SECTION:
        path_cfg = ((uci_package_t *)(((uci_section_t *)(delta->father))->father))->name;
        path_sec = ((uci_section_t *)(delta->father))->name;
        if(sublevel) path_elm = delta->name;
        break;
    case UCI_DELTA_ELEMENT:
        path_cfg = ((uci_package_t *)(((uci_section_t *)(((uci_element_t *)(delta->father))->father))->father))->name;
        path_sec = ((uci_section_t *)(((uci_element_t *)(delta->father))->father))->name;
        path_elm = delta->name;
        break;
    default:
        break;
    }

    switch (delta->cmd)
    {
    case UCI_MOD_CMD_REMOVE:    prefix = "-";break;
    case UCI_MOD_CMD_RENAME:    prefix = "@";break;
    case UCI_MOD_CMD_ADD:       prefix = "+";break;
    case UCI_MOD_CMD_REORDER:   prefix = "^";break;
    case UCI_MOD_CMD_ADD_LIST:  prefix = "|";break;
    case UCI_MOD_CMD_NONE:
    default:
        break;
    }

    fprintf(file, "%s", prefix);
    if(path_cfg) fprintf(file, "%s", path_cfg);
    if(path_sec) fprintf(file, ".%s", path_sec);
    if(path_elm) fprintf(file, ".%s", path_elm);

    // if(delta->cmd != UCI_MOD_CMD_REMOVE)
    if(delta->value)
        fprintf(file, "='%s'\n", delta->value);
    else
        fprintf(file, "\n");
}

uci_void uci_saveDeltaFile(uci_list_t *uci_list, const uci_char *file_name)
{
    uci_char *header, *payload;
    uci_package_t *pkg;
    uci_section_t *sec;
    uci_element_t *elm;
    uci_delta_t *delta;
    uci_file *file;

    UCI_PARMS_ASSERT(uci_list && file_name);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    file = uci_openFileStream(file_name, uci_list->save_path, SEEK_SET, true, true);
    if(!file) UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);


    UCI_LIST_FOREACH(&uci_list->package, pkg) if(!strcmp(pkg->name, file_name)) break;
    UCI_LIST_FOREACH(&uci_list->delta, delta) if(!strcmp(delta->name, file_name)) uci_saveDeltaNode(delta, file);

    UCI_LIST_FOREACH(&pkg->delta, delta)
        uci_saveDeltaNode(delta, file);

    UCI_LIST_FOREACH(&pkg->section, sec)
    {
        UCI_LIST_FOREACH(&sec->delta, delta)
            uci_saveDeltaNode(delta, file);

        UCI_LIST_FOREACH(&sec->element, elm)
            UCI_LIST_FOREACH(&elm->delta, delta)
                uci_saveDeltaNode(delta, file);
    }

    uci_closeFileStream(file);
}

uci_bool uci_parseDeltaLine(uci_list_t *uci_list, uci_char *line_buff)
{
    uci_char *header, *payload, *token, *end, *last_word;
    uci_delta_t *delta;
    uci_int token_idx;
    uci_package_t *pkg;
    uci_section_t *sec;
    uci_element_t *elm;
    uci_bool sublevel = false;

    UCI_PARMS_ASSERT(uci_list && line_buff);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_FALSE);

    /* ����ת�� */
    if(line_buff[strlen(line_buff)-1] = '\n') line_buff[strlen(line_buff)-1] = 0;

    header = line_buff;
    payload = line_buff+1;

    delta = uci_malloc_memory(sizeof(uci_delta_t));

    switch (header[0])
    {
    case '-': delta->cmd = UCI_MOD_CMD_REMOVE;break;
    case '+': delta->cmd = UCI_MOD_CMD_ADD;break;
    case '@': delta->cmd = UCI_MOD_CMD_RENAME;break;
    case '^': delta->cmd = UCI_MOD_CMD_REORDER;break;
    case '|': delta->cmd = UCI_MOD_CMD_ADD_LIST;break;
    default:  delta->cmd = UCI_MOD_CMD_NONE;goto end;
    }

    if(delta->cmd == UCI_MOD_CMD_ADD || delta->cmd == UCI_MOD_CMD_ADD_LIST || delta->cmd == UCI_MOD_CMD_REMOVE) sublevel = true;

    if((token = strchr(payload,'=')))
    {
        token[0] = 0;
        token++;
        uci_deleteQuotes(token, token);
        delta->value = uci_malloc_string(token);
    }

    token_idx = UCI_CMDLINE_CFG_FILE;
    token = strtok_r(payload, ".", &end);

    while (token)
    {
        switch (token_idx)
        {
        case UCI_CMDLINE_CFG_FILE:
            UCI_LIST_FOREACH(&uci_list->package, pkg)
                if(!strcmp(pkg->name, token)) break;
            if(UCI_LIST_FOREACH_END(&uci_list->package, pkg) && !sublevel) goto end;
            else last_word = token;
            break;
        case UCI_CMDLINE_SECTION:
            UCI_LIST_FOREACH(&pkg->section, sec)
                if(!strcmp(sec->name, token)) break;
            if(UCI_LIST_FOREACH_END(&pkg->section, sec) && !sublevel) goto end;
            else last_word = token;
            break;
        case UCI_CMDLINE_OPTION:
            UCI_LIST_FOREACH(&sec->element, elm)
                if(!strcmp(elm->name, token)) break;
            if(UCI_LIST_FOREACH_END(&sec->element, elm) && !sublevel) goto end;
            else last_word = token;
            break;
        default:
            break;
        }
        token = strtok_r(NULL, ".", &end);
        token_idx++;
    }

    /* 抵消最后一次自加 */
    token_idx--;

    if(token_idx == UCI_CMDLINE_CFG_FILE)       delta->name = uci_malloc_string(last_word);
    else if(token_idx == UCI_CMDLINE_SECTION)   delta->name = uci_malloc_string(last_word);
    else if(token_idx == UCI_CMDLINE_OPTION)    delta->name = uci_malloc_string(last_word);

    /* 应该在父级进行的操作 */
    if(sublevel) token_idx--;

    /* 链接delta */
    if(token_idx == UCI_CMDLINE_PROCESS)        uci_addDelta(uci_list, delta, UCI_DELTA_LIST);
    else if(token_idx == UCI_CMDLINE_CFG_FILE)  uci_addDelta(pkg, delta, UCI_DELTA_PACKAGE);
    else if(token_idx == UCI_CMDLINE_SECTION)   uci_addDelta(sec, delta, UCI_DELTA_SECTION);
    else if(token_idx == UCI_CMDLINE_OPTION)    uci_addDelta(elm, delta, UCI_DELTA_ELEMENT);

end:
    if(delta->cmd == UCI_MOD_CMD_NONE)
    {
        if(delta->name) uci_free_memory(delta->name);
        if(delta->value) uci_free_memory(delta->value);
        uci_free_memory(delta);
        delta = NULL;
        return false;
    }

    return true;
}

uci_void uci_parseDeltaFile(uci_list_t *uci_list, const uci_char *file_name)
{
    uci_char *line_buff;
    uci_char *header;
    uci_int line_buff_size = UCI_BUFFER_SIZE;
    uci_file *file;

    UCI_PARMS_ASSERT(uci_list && file_name);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    file = uci_openFileStream(file_name, uci_list->save_path, SEEK_SET, false, false);
    if(!file) return;   /* no changes */

    line_buff = uci_malloc_memory(line_buff_size);
    do
    {
        header = fgets(line_buff, line_buff_size, file);
        if(!header || !header[0]) break;
        uci_parseDeltaLine(uci_list, header);
    } while (1);
    
    uci_closeFileStream(file);
    uci_free_memory(line_buff);
}

uci_void uci_parseDeltaDir(uci_list_t *uci_list)
{
    uci_char *dir;
    uci_char *file_name;
    uci_int size, idx;
	glob_t globbuf;

    UCI_PARMS_ASSERT(uci_list && uci_list->save_path);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    dir = uci_malloc_memory(strlen(uci_list->save_path)+3);

    if(uci_list->save_path[strlen(uci_list->save_path)-1] == '/')
        sprintf(dir, "%s*", uci_list->save_path);
    else
        sprintf(dir, "%s/*", uci_list->save_path);

    if(glob(dir, GLOB_MARK, NULL, &globbuf))
    {
        uci_free_memory(dir);
        UCI_DEBUG_ERROR(UCI_ERROR_DIR_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
    }

    for(idx = 0; idx < globbuf.gl_pathc; idx++)
    {
        file_name = get_filename(globbuf.gl_pathv[idx]);
        if(!file_name) continue;
        if(!uci_validateString(file_name, true)) continue;
        uci_parseDeltaFile(uci_list, file_name);
    }
    
    uci_free_memory(dir);
    globfree(&globbuf);
}

uci_void uci_showDelta(uci_delta_t *delta)
{
    uci_char *prefix = "";
    uci_char *path_cfg = NULL;
    uci_char *path_sec = NULL;
    uci_char *path_elm = NULL;
    uci_bool sublevel = false;

    UCI_PARMS_ASSERT(delta && delta->father && delta->cmd);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    if(delta->cmd == UCI_MOD_CMD_ADD || delta->cmd == UCI_MOD_CMD_REMOVE || delta->cmd == UCI_MOD_CMD_ADD_LIST)
        sublevel = true;

    switch (delta->type)
    {
    case UCI_DELTA_LIST:
        path_cfg = delta->name;
        break;
    case UCI_DELTA_PACKAGE:
        path_cfg = ((uci_package_t *)(delta->father))->name;
        if(sublevel) path_sec = delta->name;
        break;
    case UCI_DELTA_SECTION:
        path_cfg = ((uci_package_t *)(((uci_section_t *)(delta->father))->father))->name;
        path_sec = ((uci_section_t *)(delta->father))->name;
        if(sublevel) path_elm = delta->name;
        break;
    case UCI_DELTA_ELEMENT:
        path_cfg = ((uci_package_t *)(((uci_section_t *)(((uci_element_t *)(delta->father))->father))->father))->name;
        path_sec = ((uci_section_t *)(((uci_element_t *)(delta->father))->father))->name;
        path_elm = delta->name;
        break;
    default:
        break;
    }

    switch (delta->cmd)
    {
    case UCI_MOD_CMD_REMOVE:    prefix = "-";break;
    case UCI_MOD_CMD_RENAME:    prefix = "@";break;
    case UCI_MOD_CMD_ADD:       prefix = "+";break;
    case UCI_MOD_CMD_REORDER:   prefix = "^";break;
    case UCI_MOD_CMD_ADD_LIST:  prefix = "|";break;
    case UCI_MOD_CMD_NONE:
    default:
        break;
    }

    UCI_OUTPUT("%s", prefix);
    if(path_cfg) UCI_OUTPUT("%s", path_cfg);
    if(path_sec) UCI_OUTPUT(".%s", path_sec);
    if(path_elm) UCI_OUTPUT(".%s", path_elm);

    // if(delta->cmd!=UCI_MOD_CMD_REMOVE) UCI_OUTPUT("=%s\n", delta->value);
    if(delta->value) UCI_OUTPUT("=%s\n", delta->value);
    else UCI_OUTPUT("\n");
}

static uci_bool uci_mergeDeltaList(uci_list_t *uci_list, uci_delta_t *uci_delta)
{
    uci_package_t *uci_pkg;

    UCI_PARMS_ASSERT(uci_list && uci_delta);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_FALSE);

    switch (uci_delta->cmd)
    {
    case UCI_MOD_CMD_REMOVE:
        UCI_LIST_FOREACH(&uci_list->package, uci_pkg)
            if(!strcmp(uci_pkg->name, uci_delta->name))
            {
                uci_deleteFile(uci_pkg->name, uci_list->conf_path);
                uci_deleteFile(uci_pkg->name, uci_list->save_path);
                uci_release_package(uci_pkg);
                break;
            }
        break;
    case UCI_MOD_CMD_ADD:    
        UCI_LIST_FOREACH(&uci_list->package, uci_pkg)
			if(!strcmp(uci_pkg->name, uci_delta->name))
				uci_release_package(uci_pkg);

        uci_pkg = uci_new_package(uci_delta->name);
        uci_insertList(uci_list, uci_pkg);
        break;
    default:
        break;
    }

    return true;
}

static uci_bool uci_mergeDeltaPackge(uci_package_t *uci_package, uci_delta_t *uci_delta)
{
    uci_section_t *uci_sec;
    uci_package_t *uci_pkg;

    UCI_PARMS_ASSERT(uci_package && uci_delta);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_FALSE);

    uci_pkg = uci_package;

    switch (uci_delta->cmd)
    {
    case UCI_MOD_CMD_REMOVE:
        UCI_LIST_FOREACH(&uci_package->section, uci_sec)
            if(!strcmp(uci_sec->name, uci_delta->name))
                uci_release_section(uci_sec);

        break;
    case UCI_MOD_CMD_ADD:
        UCI_LIST_FOREACH(&uci_package->section, uci_sec)
			if(!strcmp(uci_sec->name, uci_delta->name))
				uci_release_section(uci_sec);

        uci_sec = uci_new_section(uci_delta->name, uci_delta->value);
        uci_insertPackage(uci_package, uci_sec);
        break;
    case UCI_MOD_CMD_RENAME:
        if(!strcmp(uci_pkg->name, uci_delta->name))
        {
            uci_free_memory(uci_pkg->name);
            uci_pkg->name = uci_malloc_string(uci_delta->value);
        }
        break;
    default:
        break;
    }

    return true;
}

static uci_bool uci_mergeDeltaSection(uci_section_t *uci_section, uci_delta_t *uci_delta)
{
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;

    UCI_PARMS_ASSERT(uci_section && uci_delta);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_FALSE);

    uci_sec = uci_section;

    switch (uci_delta->cmd)
    {
    case UCI_MOD_CMD_REMOVE:
        UCI_LIST_FOREACH(&uci_section->element, uci_elm)
            if(!strcmp(uci_elm->name, uci_delta->name))
                if(!uci_delta->value)
                    uci_release_element(uci_elm);
                else
                {
                    if(!strcmp(uci_elm->value, uci_delta->value))
                    {
                        uci_release_element(uci_elm);
                        break;
                    }
                }
        break;
    case UCI_MOD_CMD_ADD:
        /* ɾ������ͬ��Ԫ�� */
        UCI_LIST_FOREACH(&uci_section->element, uci_elm)
            if(!strcmp(uci_elm->name, uci_delta->name))
                uci_release_element(uci_elm);

        uci_elm = uci_new_element(uci_delta->name, uci_delta->value);
        uci_elm->is_list = false;
        uci_insertSection(uci_section, uci_elm);
        break;
    case UCI_MOD_CMD_ADD_LIST:
        uci_elm = uci_new_element(uci_delta->name, uci_delta->value);
        uci_elm->is_list = true;
        uci_insertSection(uci_section, uci_elm);
        break;
    case UCI_MOD_CMD_RENAME:
        if(!strcmp(uci_sec->name, uci_delta->name))
        {
            uci_free_memory(uci_sec->name);
            uci_sec->name = uci_malloc_string(uci_delta->value);
        }
        break;
    case UCI_MOD_CMD_REORDER:
        if(!strcmp(uci_sec->name, uci_delta->name))
        {
            uci_free_memory(uci_sec->type);
            uci_sec->type = uci_malloc_string(uci_delta->value);
        }
        break;
    default:
        break;
    }

    return true;
}

static uci_bool uci_mergeDeltaElement(uci_element_t *uci_element, uci_delta_t *uci_delta)
{
    uci_element_t *uci_elm;

    UCI_PARMS_ASSERT(uci_element && uci_delta);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_FALSE);

    uci_elm = uci_element;

    switch (uci_delta->cmd)
    {
    case UCI_MOD_CMD_RENAME:
        if(!strcmp(uci_elm->name, uci_delta->name))
        {
            uci_free_memory(uci_elm->name);
            uci_elm->name = uci_malloc_string(uci_delta->value);
        }
    case UCI_MOD_CMD_REORDER:
        if(!strcmp(uci_elm->name, uci_delta->name))
        {
            uci_free_memory(uci_elm->value);
            uci_elm->value = uci_malloc_string(uci_delta->value);
        }
        break;
    default:
        break;
    }

    return true;
}

static uci_bool uci_mergeDelta(uci_delta_t *uci_delta)
{
    uci_void *father;
    uci_bool ret = false;

    UCI_PARMS_ASSERT(uci_delta);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_FALSE);

    father = uci_delta->father;

    switch (uci_delta->type)
    {
    case UCI_DELTA_LIST:
        ret = uci_mergeDeltaList((uci_list_t *)father, uci_delta);
        break;
    case UCI_DELTA_PACKAGE:
        ret = uci_mergeDeltaPackge((uci_package_t *)father, uci_delta);
        break;
    case UCI_DELTA_SECTION:
        ret = uci_mergeDeltaSection((uci_section_t *)father, uci_delta);
        break;
    case UCI_DELTA_ELEMENT:
        ret = uci_mergeDeltaElement((uci_element_t *)father, uci_delta);
        break;
    default:
        break;
    }

    return ret;
}

uci_void uci_mergeChanges(uci_list_t *uci_list)
{
    uci_delta_t *uci_delta;
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;

    UCI_PARMS_ASSERT(uci_list);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_list->delta, uci_delta)
    {
        if(uci_delta) uci_mergeDelta(uci_delta);
    }

    UCI_LIST_FOREACH(&uci_list->package, uci_pkg)
    {
        UCI_LIST_FOREACH(&uci_pkg->delta, uci_delta)
            if(uci_delta) uci_mergeDelta(uci_delta);

        UCI_LIST_FOREACH(&uci_pkg->section, uci_sec)
        {
            UCI_LIST_FOREACH(&uci_sec->delta, uci_delta)
                if(uci_delta) uci_mergeDelta(uci_delta);

            UCI_LIST_FOREACH(&uci_sec->element, uci_elm)
                UCI_LIST_FOREACH(&uci_elm->delta, uci_delta)
                    if(uci_delta) uci_mergeDelta(uci_delta);
        }
    }
}

uci_void uci_commitFile(uci_package_t *uci_package, const uci_char *dir)
{
    uci_file *file;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;

    UCI_PARMS_ASSERT(uci_package);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    file = uci_openFileStream(uci_package->name, dir, SEEK_SET, true, true);
    if(!file) UCI_DEBUG_ERROR(UCI_ERROR_FILE_IO);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    fprintf(file, "package %s\n\n", uci_package->name);

    UCI_LIST_FOREACH(&uci_package->section, uci_sec)
    {
        if(uci_sec->name[0] == '\'' || uci_sec->name[0] == '"')
            fprintf(file, "config\t%s\t%s\n", uci_sec->type, uci_sec->name);
        else
            fprintf(file, "config\t%s\t'%s'\n", uci_sec->type, uci_sec->name);

        UCI_LIST_FOREACH(&uci_sec->element, uci_elm)
        {
            if(uci_elm->is_list == true)
            {
                if(uci_elm->value[0] == '\'' || uci_elm->value[0] == '"') 
                    fprintf(file, "\tlist\t%s\t%s\n", uci_elm->name, uci_elm->value);
                else 
                    fprintf(file, "\tlist\t%s\t'%s'\n", uci_elm->name, uci_elm->value);
            }
            else
            {
                if(uci_elm->value[0] == '\'' || uci_elm->value[0] == '"') 
                    fprintf(file, "\toption\t%s\t%s\n", uci_elm->name, uci_elm->value);
                else 
                    fprintf(file, "\toption\t%s\t'%s'\n", uci_elm->name, uci_elm->value);
            }
        }

        fprintf(file,"\n");
    }

    uci_closeFileStream(file);
}

uci_void uci_commitFiles(uci_list_t *uci_list)
{
    uci_package_t *uci_pkg;

    UCI_PARMS_ASSERT(uci_list);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_list->package, uci_pkg)
    {
        uci_commitFile(uci_pkg, uci_list->conf_path);
        uci_deleteFile(uci_pkg->name, uci_list->save_path);
    }
}
