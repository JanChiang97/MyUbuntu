/************************************************************************************************
 ** @File:    uci_function.c
 ** @Author:  Jan
 ** @Date:    2024/01/04
 ** @Brief:   TThis file provides the API for uci_function
 ************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "uci_function.h"
/* Public variables --------------------------------------------------------*/
/* Public macros -----------------------------------------------------------*/
/* Public typedef ----------------------------------------------------------*/
/* Public functions --------------------------------------------------------*/

uci_void uci_runCommand(uci_parameter_t *uci_parms)
{
    uci_cmd_list[uci_parms->command_type].callback(uci_parms);
}

/*
 * Callbck function for uci command
 */
uci_void uci_runHelpCommand(uci_parameter_t *uci_parms)
{
    (void)uci_parms;

    LOG_PRINT("Usage: uci <command> [<arguments>]");
    LOG_PRINT("Command List:");
    for(uci_command_type_t cmd = 0; cmd < UCI_COMMAND_MAX; cmd++)
    {
        LOG_PRINT("%-16s %s",uci_cmd_list[cmd].match_string, uci_cmd_list[cmd].description);
    }
}

uci_void uci_runShowCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list; 
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;

    UCI_PARMS_ASSERT(uci_parms && uci_parms->command_type == UCI_COMMAND_SHOW);

    uci_list = uci_new_list();
    
    if(uci_parms->command_target == UCI_CMDLINE_PROCESS) uci_parseConfigDir(uci_list);
    else uci_parseConfigFile(uci_list, uci_parms->config);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
    
    if(uci_parms->command_target == UCI_CMDLINE_CFG_FILE)
    {
        UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_parms->config, uci_pkg->name)) break;
        if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))  UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

        UCI_LIST_FOREACH(&uci_pkg->section, uci_sec)
        {
            UCI_OUTPUT("%s.%s=%s\n", uci_parms->config, uci_sec->name, uci_sec->type);
            UCI_LIST_FOREACH(&uci_sec->element, uci_elm)
            {
                UCI_OUTPUT("%s.%s.%s='%s'\n", uci_parms->config, uci_sec->name, uci_elm->name, uci_elm->value);
            }
        }
    }
    else if(uci_parms->command_target == UCI_CMDLINE_SECTION)
    {
        UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_parms->config, uci_pkg->name)) break;
        if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))  UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

        UCI_LIST_FOREACH(&uci_pkg->section, uci_sec) if(!strcmp(uci_parms->section, uci_sec->name)) break;
        if(UCI_LIST_FOREACH_END(&uci_pkg->section, uci_sec))  UCI_DEBUG_ERROR(UCI_ERROR_SECTION_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

        UCI_OUTPUT("%s.%s=%s\n", uci_parms->config, uci_sec->name, uci_sec->type);
        UCI_LIST_FOREACH(&uci_sec->element, uci_elm)
            UCI_OUTPUT("%s.%s.%s='%s'\n", uci_parms->config, uci_sec->name, uci_elm->name, uci_elm->value);
    }
    else if(uci_parms->command_target == UCI_CMDLINE_OPTION)
    {
        UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_parms->config, uci_pkg->name)) break;
        if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))  UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

        UCI_LIST_FOREACH(&uci_pkg->section, uci_sec) if(!strcmp(uci_parms->section, uci_sec->name)) break;
        if(UCI_LIST_FOREACH_END(&uci_pkg->section, uci_sec))  UCI_DEBUG_ERROR(UCI_ERROR_SECTION_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

        UCI_LIST_FOREACH(&uci_sec->element, uci_elm) if(!strcmp(uci_parms->option, uci_elm->name)) break;
        if(UCI_LIST_FOREACH_END(&uci_sec->element, uci_elm))  UCI_DEBUG_ERROR(UCI_ERROR_OPTION_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

        UCI_OUTPUT("%s.%s.%s='%s'\n", uci_parms->config, uci_sec->name, uci_elm->name, uci_elm->value);
    }
    else
    {
        UCI_LIST_FOREACH(&uci_list->package, uci_pkg)
        {
            UCI_LIST_FOREACH(&uci_pkg->section, uci_sec)
            {
                UCI_OUTPUT("%s.%s=%s\n", uci_pkg->name, uci_sec->name, uci_sec->type);
                UCI_LIST_FOREACH(&uci_sec->element, uci_elm)
                    UCI_OUTPUT("%s.%s.%s='%s'\n", uci_pkg->name, uci_sec->name, uci_elm->name, uci_elm->value);
            }
        }
    }

end:
    uci_release_list(uci_list);
}

uci_void uci_runSetCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list; 
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;
    uci_delta_t *uci_delta;

    UCI_PARMS_ASSERT(uci_parms && uci_parms->set_value && uci_parms->command_type == UCI_COMMAND_SET);

    if(uci_parms->command_target != UCI_CMDLINE_OPTION && uci_parms->command_target != UCI_CMDLINE_SECTION) 
        UCI_DEBUG_ERROR(UCI_ERROR_REQUREST);

    uci_list = uci_new_list();

    /* 只需解析对应文件 */
    uci_parseConfigFile(uci_list, uci_parms->config);
    uci_parseDeltaFile(uci_list, uci_parms->config);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_pkg->name, uci_parms->config)) break;
    if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))     UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_pkg->section, uci_sec)  if(!strcmp(uci_sec->name, uci_parms->section)) break;
    if(UCI_LIST_FOREACH_END(&uci_pkg->section, uci_sec))
    {
        if(uci_parms->command_target == UCI_CMDLINE_OPTION)  UCI_DEBUG_ERROR(UCI_ERROR_SECTION_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
#if UCI_DIRECT_ADD_SECTION_ENABLED
        if(uci_parms->command_target == UCI_CMDLINE_SECTION) 
        {
            uci_section_t* uci_sec_new = uci_new_section(uci_parms->section, uci_parms->set_value);
            uci_insertPackage(uci_pkg, uci_sec_new);
            uci_commitFile(uci_pkg, uci_list->conf_path);
        }
#else
        uci_delta = uci_new_delta(uci_parms->section, uci_parms->set_value, UCI_MOD_CMD_ADD);
        uci_addDelta(uci_pkg, uci_delta, UCI_DELTA_PACKAGE);
#endif
        goto done;
    }

    if(uci_parms->command_target == UCI_CMDLINE_OPTION)
    {
        UCI_LIST_FOREACH(&uci_sec->element, uci_elm)
        {
            if(!strcmp(uci_elm->name, uci_parms->option))
            {
                uci_delta = uci_new_delta(uci_parms->option, uci_parms->set_value, UCI_MOD_CMD_REORDER);
                uci_addDelta(uci_elm, uci_delta, UCI_DELTA_ELEMENT);
                goto done;
            }
        }

        uci_delta = uci_new_delta(uci_parms->option, uci_parms->set_value, UCI_MOD_CMD_ADD);
        uci_addDelta(uci_sec, uci_delta, UCI_DELTA_SECTION);
        goto done;
    }
    else
    {
        uci_delta = uci_new_delta(uci_parms->section, uci_parms->set_value, UCI_MOD_CMD_REORDER);
        uci_addDelta(uci_sec, uci_delta, UCI_DELTA_SECTION);
        goto done;
    }
    
done:
    uci_saveDeltaFile(uci_list, uci_parms->config);
    uci_release_list(uci_list);
}

uci_void uci_runGetCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list;
    uci_bool    isListFound = false;
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;
    uci_delta_t *uci_delta;

    UCI_PARMS_ASSERT(uci_parms && uci_parms->command_type == UCI_COMMAND_GET);

    if(uci_parms->command_target != UCI_CMDLINE_OPTION && uci_parms->command_target != UCI_CMDLINE_SECTION) 
        UCI_DEBUG_ERROR(UCI_ERROR_REQUREST);

    uci_list = uci_new_list();

    /* 只需解析对应文件 */
    uci_parseConfigFile(uci_list, uci_parms->config);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_pkg->name, uci_parms->config)) break;
    if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))    UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_pkg->section, uci_sec) if(!strcmp(uci_sec->name, uci_parms->section)) break;
    if(UCI_LIST_FOREACH_END(&uci_pkg->section, uci_sec))     UCI_DEBUG_ERROR(UCI_ERROR_SECTION_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    if(uci_parms->command_target == UCI_CMDLINE_SECTION)
    {
        uci_parms->ret_value = uci_malloc_string(uci_sec->type);
        UCI_OUTPUT("%s\n", uci_sec->type);
        goto found;
    }
    else if(uci_parms->command_target == UCI_CMDLINE_OPTION)
    {
        UCI_LIST_FOREACH(&uci_sec->element, uci_elm)
        {
            if(!strcmp(uci_elm->name, uci_parms->option))
            {
                if(!uci_parms->ret_value) uci_parms->ret_value = uci_malloc_string(uci_sec->type);
                else
                {
                    uci_uint32 len = strlen(uci_parms->ret_value);
                    uci_parms->ret_value[len] = '\n';
                    uci_parms->ret_value[len+1] = 0;
                    uci_parms->ret_value = uci_realloc_memory(uci_parms->ret_value, strlen(uci_parms->ret_value)+strlen(uci_sec->type)+2);
                }
                UCI_OUTPUT("%s\n", uci_elm->value);
                if(uci_elm->is_list)
                    isListFound = true;
                else
                    goto found;
            }
        }

        if(isListFound) goto found;
        
        UCI_DEBUG_ERROR(UCI_ERROR_OPTION_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
    }

found:
    uci_release_list(uci_list);
}

uci_void uci_runAddCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list;

	uci_file *file = NULL;

    UCI_PARMS_ASSERT(uci_parms && uci_parms->command_type == UCI_COMMAND_ADD);

#if 1
    if(uci_parms->command_target != UCI_CMDLINE_CFG_FILE) UCI_DEBUG_ERROR(UCI_ERROR_REQUREST);

    uci_list = uci_new_list();

    file = uci_openFileStream(uci_parms->config, uci_list->conf_path, SEEK_SET, true, true);
    if(!file) UCI_DEBUG_ERROR(UCI_ERROR_FILE_IO);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    uci_closeFileStream(file);
    uci_release_list(uci_list);
#else
    uci_list = uci_new_list();
    uci_parseConfigFile(uci_list, uci_parms->config);
    uci_parseDeltaFile(uci_list, uci_parms->config);
    
    UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_pkg->name, uci_parms->config)) break;
    if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))     UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);

    UCI_LIST_FOREACH(&uci_pkg->section, uci_sec) if(!strcmp(uci_sec->type, uci_parms->ret_value)) ano_id++;

    ano_name = uci_malloc_memory(strlen(uci_parms->ret_value)+5);

    snprintf(ano_name, strlen(uci_parms->ret_value)+5, "@%s[%d]", uci_parms->ret_value, ano_id);

    uci_delta = uci_new_delta(ano_name, uci_parms->ret_value, UCI_MOD_CMD_ADD);
    uci_addDelta(uci_pkg, uci_delta, UCI_DELTA_PACKAGE);

    uci_free_memory(ano_name);
    uci_saveDeltaFile(uci_list, uci_parms->config);
    uci_release_list(uci_list);
#endif
}

uci_void uci_runAddListCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list;
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;
    uci_delta_t *uci_delta;
    
    UCI_PARMS_ASSERT(uci_parms && uci_parms->command_type == UCI_COMMAND_ADD_LIST);

    uci_list = uci_new_list();
    uci_parseConfigFile(uci_list, uci_parms->config);
    uci_parseDeltaFile(uci_list, uci_parms->config);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_pkg->name, uci_parms->config)) break;
    if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))     UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_pkg->section, uci_sec) if(!strcmp(uci_sec->name, uci_parms->section)) break;
    if(UCI_LIST_FOREACH_END(&uci_pkg->section, uci_sec))     UCI_DEBUG_ERROR(UCI_ERROR_SECTION_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    uci_delta = uci_new_delta(uci_parms->option, uci_parms->set_value, UCI_MOD_CMD_ADD_LIST);
    uci_addDelta(uci_sec, uci_delta, UCI_DELTA_SECTION);

    uci_saveDeltaFile(uci_list, uci_parms->config);
    uci_release_list(uci_list);
}

uci_void uci_runDelListCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list;
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;
    uci_delta_t *uci_delta;

    UCI_PARMS_ASSERT(uci_parms && uci_parms->command_type == UCI_COMMAND_DEL_LIST);

    if(uci_parms->command_target != UCI_CMDLINE_OPTION || uci_parms->set_value == NULL) UCI_DEBUG_ERROR(UCI_ERROR_REQUREST);

    uci_list = uci_new_list();
    uci_parseConfigFile(uci_list, uci_parms->config);
    uci_parseDeltaFile(uci_list, uci_parms->config);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_pkg->name, uci_parms->config)) break;
    if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))     UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_pkg->section, uci_sec) if(!strcmp(uci_sec->name, uci_parms->section)) break;
    if(UCI_LIST_FOREACH_END(&uci_pkg->section, uci_sec))     UCI_DEBUG_ERROR(UCI_ERROR_SECTION_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_sec->element, uci_elm) 
        if( !strcmp(uci_elm->name, uci_parms->option) && 
            !strcmp(uci_elm->value, uci_parms->set_value) &&
            uci_elm->is_list) break;
            
    if(UCI_LIST_FOREACH_END(&uci_sec->element, uci_elm))     UCI_DEBUG_ERROR(UCI_ERROR_OPTION_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    uci_delta = uci_new_delta(uci_parms->option, uci_parms->set_value, UCI_MOD_CMD_REMOVE);
    uci_addDelta(uci_sec, uci_delta, UCI_DELTA_SECTION);

    uci_saveDeltaFile(uci_list, uci_parms->config);
    uci_release_list(uci_list);
}

uci_void uci_runDelCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list;
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;
    uci_delta_t *uci_delta;

    UCI_PARMS_ASSERT(uci_parms && uci_parms->command_type == UCI_COMMAND_DEL);

    if(uci_parms->command_target == UCI_CMDLINE_PROCESS) UCI_DEBUG_ERROR(UCI_ERROR_REQUREST);

    uci_list = uci_new_list();

    /* 只需解析对应文件 */
    uci_parseConfigFile(uci_list, uci_parms->config);
    uci_parseDeltaFile(uci_list, uci_parms->config);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_pkg->name, uci_parms->config)) break;
    if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))     UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    if(uci_parms->command_target == UCI_CMDLINE_CFG_FILE)
    {
        uci_delta = uci_new_delta(uci_parms->config, uci_parms->set_value, UCI_MOD_CMD_REMOVE);
        uci_addDelta(uci_list, uci_delta, UCI_DELTA_LIST);
        goto done;
    }
    else if(uci_parms->command_target == UCI_CMDLINE_SECTION)
    {
        UCI_LIST_FOREACH(&uci_pkg->section, uci_sec) if(!strcmp(uci_sec->name, uci_parms->section)) break;
        if(UCI_LIST_FOREACH_END(&uci_pkg->section, uci_sec))     UCI_DEBUG_ERROR(UCI_ERROR_SECTION_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

        uci_delta = uci_new_delta(uci_parms->section, uci_parms->set_value, UCI_MOD_CMD_REMOVE);
        uci_addDelta(uci_pkg, uci_delta, UCI_DELTA_PACKAGE);
        goto done;
    }
    else if(uci_parms->command_target == UCI_CMDLINE_OPTION)
    {
        UCI_LIST_FOREACH(&uci_pkg->section, uci_sec) if(!strcmp(uci_sec->name, uci_parms->section)) break;
        if(UCI_LIST_FOREACH_END(&uci_pkg->section, uci_sec))     UCI_DEBUG_ERROR(UCI_ERROR_SECTION_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

        UCI_LIST_FOREACH(&uci_sec->element, uci_elm) if(!strcmp(uci_elm->name, uci_parms->option)) break;
        if(UCI_LIST_FOREACH_END(&uci_sec->element, uci_elm))     UCI_DEBUG_ERROR(UCI_ERROR_OPTION_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

        uci_delta = uci_new_delta(uci_parms->option, uci_parms->set_value, UCI_MOD_CMD_REMOVE);
        uci_addDelta(uci_sec, uci_delta, UCI_DELTA_SECTION);
        goto done;
    }
    
done:
    uci_saveDeltaFile(uci_list, uci_parms->config);
    uci_release_list(uci_list);
}

uci_void uci_runRenameCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list;
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;
    uci_delta_t *uci_delta;

    UCI_PARMS_ASSERT(uci_parms && uci_parms->command_type == UCI_COMMAND_RENAME);

    if(uci_parms->command_target != UCI_CMDLINE_OPTION && uci_parms->command_target != UCI_CMDLINE_SECTION) 
        UCI_DEBUG_ERROR(UCI_ERROR_REQUREST);

    uci_list = uci_new_list();

    /* 只需解析对应文件 */
    uci_parseConfigFile(uci_list, uci_parms->config);
    uci_parseDeltaFile(uci_list, uci_parms->config);

    UCI_LIST_FOREACH(&uci_list->package, uci_pkg) if(!strcmp(uci_pkg->name, uci_parms->config)) break;
    if(UCI_LIST_FOREACH_END(&uci_list->package, uci_pkg))     UCI_DEBUG_ERROR(UCI_ERROR_CFG_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_pkg->section, uci_sec)  if(!strcmp(uci_sec->name, uci_parms->section)) break;
    if(UCI_LIST_FOREACH_END(&uci_pkg->section, uci_sec))      UCI_DEBUG_ERROR(UCI_ERROR_SECTION_NOT_FOUND);
    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    if(uci_parms->command_target == UCI_CMDLINE_OPTION)
    {
        UCI_LIST_FOREACH(&uci_sec->element, uci_elm)
        {
            if(!strcmp(uci_elm->name, uci_parms->option))
            {
                uci_delta = uci_new_delta(uci_parms->option, uci_parms->set_value, UCI_MOD_CMD_RENAME);
                uci_addDelta(uci_elm, uci_delta, UCI_DELTA_ELEMENT);
                goto done;
            }
        }

        UCI_DEBUG_ERROR(UCI_ERROR_OPTION_NOT_FOUND);
        UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);
    }
    else
    {
        uci_delta = uci_new_delta(uci_parms->section, uci_parms->set_value, UCI_MOD_CMD_RENAME);
        uci_addDelta(uci_sec, uci_delta, UCI_DELTA_SECTION);
        goto done;
    }

done:
    uci_saveDeltaFile(uci_list, uci_parms->config);
    uci_release_list(uci_list);
}

uci_void uci_runChangesCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list;
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;
    uci_delta_t *uci_delta;

    UCI_PARMS_ASSERT(uci_parms && uci_parms->command_type == UCI_COMMAND_CHANGES);


    if(uci_parms->command_target != UCI_CMDLINE_CFG_FILE && uci_parms->command_target != UCI_CMDLINE_PROCESS) 
        UCI_DEBUG_ERROR(UCI_ERROR_REQUREST);

    uci_list = uci_new_list();

    /* ֻ��Ҫ������Ӧ�ļ� */
    if(uci_parms->command_target == UCI_CMDLINE_CFG_FILE)
    {
        uci_parseConfigFile(uci_list, uci_parms->config);
        uci_parseDeltaFile(uci_list, uci_parms->config);
    }
    else
    {
        uci_parseConfigDir(uci_list);
        uci_parseDeltaDir(uci_list);
    }

    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    UCI_LIST_FOREACH(&uci_list->delta, uci_delta)
        if(uci_delta) uci_showDelta(uci_delta);

    UCI_LIST_FOREACH(&uci_list->package, uci_pkg)
    {
        UCI_LIST_FOREACH(&uci_pkg->delta, uci_delta)
            if(uci_delta) uci_showDelta(uci_delta);

        UCI_LIST_FOREACH(&uci_pkg->section, uci_sec)
        {
            UCI_LIST_FOREACH(&uci_sec->delta, uci_delta)
                if(uci_delta) uci_showDelta(uci_delta);

            UCI_LIST_FOREACH(&uci_sec->element, uci_elm)
                UCI_LIST_FOREACH(&uci_elm->delta, uci_delta)
                    if(uci_delta) uci_showDelta(uci_delta);
        }
    }

    uci_release_list(uci_list);
}

uci_void uci_runCommitCommand(uci_parameter_t *uci_parms)
{
    uci_list_t *uci_list;
    uci_package_t *uci_pkg;
    uci_section_t *uci_sec;
    uci_element_t *uci_elm;
    uci_delta_t *uci_delta;

    UCI_PARMS_ASSERT(uci_parms && uci_parms->command_type == UCI_COMMAND_COMMIT);

    if(uci_parms->command_target != UCI_CMDLINE_CFG_FILE && uci_parms->command_target != UCI_CMDLINE_PROCESS) 
        UCI_DEBUG_ERROR(UCI_ERROR_REQUREST);

    uci_list = uci_new_list();

    if(uci_parms->command_target == UCI_CMDLINE_CFG_FILE)
    {
        uci_parseConfigFile(uci_list, uci_parms->config);
        uci_parseDeltaFile(uci_list, uci_parms->config);
    }
    else
    {
        uci_parseConfigDir(uci_list);
        uci_parseDeltaDir(uci_list);
    }

    UCI_ERROR_ASSERT(UCI_ERROR_EXEC_RETURN_VOID);

    uci_mergeChanges(uci_list);
    uci_commitFiles(uci_list);
    uci_release_list(uci_list);
}
/* Private variables -------------------------------------------------------*/
/* Private macros ----------------------------------------------------------*/
/* Private typedef ---------------------------------------------------------*/
/* Private functions -------------------------------------------------------*/
