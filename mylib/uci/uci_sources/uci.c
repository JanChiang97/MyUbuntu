
/************************************************************************************************
 ** @File:    uci.c
 ** @Author:  JanChiang
 ** @Date:    2024/01/04
 ** @Brief:   TThis file provides the API for uci
 ************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "uci.h"
/* Public variables --------------------------------------------------------*/
/* Public macros -----------------------------------------------------------*/
/* Public typedef ----------------------------------------------------------*/
/* Public functions --------------------------------------------------------*/
/* Private variables -------------------------------------------------------*/
/* Private macros ----------------------------------------------------------*/
/* Private typedef ---------------------------------------------------------*/
/* Private functions -------------------------------------------------------*/

/* Templates for adding functional code. */
uci_errno_t uci_new_functon(void *parms)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(parms);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    /* init start. */
    // uci_parms.command_type = XXX;
    // uci_parms.command_target = XXX;
    // uci_parms.config = XXX;
    // uci_parms.section = XXX;
    // uci_parms.option = XXX;
    // uci_parms.set_value = XXX
    /* init end */

    uci_runCommand(&uci_parms);

    /* Logic start */
    uci_parms.error_code = uci_getErrorCode();
    /* logic end */

    /* release start */
    uci_freeParameters(&uci_parms);

    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}

uci_errno_t uci_add_config(const uci_char *config)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    uci_parms.command_type = UCI_COMMAND_ADD;
    uci_parms.command_target = UCI_CMDLINE_CFG_FILE;
    uci_parms.config = uci_malloc_string(config);

    uci_runCommand(&uci_parms);
    
    uci_parms.error_code = uci_getErrorCode();

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}

uci_errno_t uci_del_config(const uci_char *config)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    uci_parms.command_type = UCI_COMMAND_DEL;
    uci_parms.command_target = UCI_CMDLINE_CFG_FILE;
    uci_parms.config = uci_malloc_string(config);

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    /* release start */
    uci_freeParameters(&uci_parms);

    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);
    /* release end */


    return uci_parms.error_code;
}

uci_errno_t uci_set_section(const uci_char *config, const uci_char *section, const uci_char *type)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section && type);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    uci_parms.command_type = UCI_COMMAND_SET;
    uci_parms.command_target = UCI_CMDLINE_SECTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);
    uci_parms.set_value = uci_malloc_string(type);

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    /* release start */
    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);
    /* release end */

    return uci_parms.error_code;
}

uci_errno_t uci_get_section(const uci_char *config, const uci_char *section, uci_char *buf, uci_uint32 size)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section && buf && size);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    uci_parms.command_type = UCI_COMMAND_GET;
    uci_parms.command_target = UCI_CMDLINE_SECTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();
    if(uci_parms.ret_value)
    {
        memset(buf, 0, size);
        memcpy(buf, uci_parms.ret_value, size>strlen(uci_parms.ret_value)?strlen(uci_parms.ret_value):size);
    }

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}

uci_errno_t uci_rename_section(const uci_char *config, const uci_char *section, const uci_char *name)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section && name);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    /* Logic code start. */
    uci_parms.command_type = UCI_COMMAND_RENAME;
    uci_parms.command_target = UCI_CMDLINE_SECTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);
    uci_parms.set_value = uci_malloc_string(name);
    /* Logic code end */

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}

uci_errno_t uci_del_section(const uci_char *config, const uci_char *section)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    /* Logic code start. */
    uci_parms.command_type = UCI_COMMAND_DEL;
    uci_parms.command_target = UCI_CMDLINE_SECTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);
    /* Logic code end */

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}


uci_errno_t uci_set_option(const uci_char *config, const uci_char *section, const uci_char *option, const uci_char *value)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section && option && value);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    /* Logic code start. */
    uci_parms.command_type = UCI_COMMAND_SET;
    uci_parms.command_target = UCI_CMDLINE_OPTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);
    uci_parms.option = uci_malloc_string(option);
    uci_parms.set_value = uci_malloc_string(value);
    /* Logic code end */

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}

uci_errno_t uci_get_option(const uci_char *config, const uci_char *section, const uci_char *option, uci_char *buf, uci_uint32 size)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section && option && buf && size);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    /* Logic code start. */
    uci_parms.command_type = UCI_COMMAND_GET;
    uci_parms.command_target = UCI_CMDLINE_OPTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);
    uci_parms.option = uci_malloc_string(option);
    /* Logic code end */

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();
    if(uci_parms.ret_value)
    {
        memset(buf, 0, size);
        memcpy(buf, uci_parms.ret_value, size>strlen(uci_parms.ret_value)?strlen(uci_parms.ret_value):size);
    }

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}


uci_errno_t uci_rename_option(const uci_char *config, const uci_char *section, const uci_char *option, const uci_char *name)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section && option && name);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    /* Logic code start. */
    uci_parms.command_type = UCI_COMMAND_RENAME;
    uci_parms.command_target = UCI_CMDLINE_OPTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);
    uci_parms.option = uci_malloc_string(option);
    uci_parms.set_value = uci_malloc_string(name);
    /* Logic code end */

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}


uci_errno_t uci_del_option(const uci_char *config, const uci_char *section, const uci_char *option)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section && option);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    /* Logic code start. */
    uci_parms.command_type = UCI_COMMAND_DEL;
    uci_parms.command_target = UCI_CMDLINE_OPTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);
    uci_parms.option = uci_malloc_string(option);
    /* Logic code end */

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}


uci_errno_t uci_add_list(const uci_char *config, const uci_char *section, const uci_char *list, const uci_char *value)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section && list && value);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    /* Logic code start. */
    uci_parms.command_type = UCI_COMMAND_ADD_LIST;
    uci_parms.command_target = UCI_CMDLINE_OPTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);
    uci_parms.option = uci_malloc_string(list);
    uci_parms.set_value = uci_malloc_string(value);
    /* Logic code end */

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}

uci_errno_t uci_get_list(const uci_char *config, const uci_char *section, const uci_char *list, uci_char *buf, uci_uint32 size)
{
    return uci_get_option(config, section, list, buf, size);
}

uci_errno_t uci_rename_list(const uci_char *config, const uci_char *section, const uci_char *list, const uci_char *name)
{
    return uci_rename_option(config, section, list, name);
}

uci_errno_t uci_del_list_all(const uci_char *config, const uci_char *section, const uci_char *list)
{
    return uci_del_option(config, section, list);
}

uci_errno_t uci_del_list(const uci_char *config, const uci_char *section, const uci_char *list, const uci_char *value)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(config && section && list && value);

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    /* Logic code start. */
    uci_parms.command_type = UCI_COMMAND_DEL_LIST;
    uci_parms.command_target = UCI_CMDLINE_OPTION;
    uci_parms.config = uci_malloc_string(config);
    uci_parms.section = uci_malloc_string(section);
    uci_parms.option = uci_malloc_string(list);
    uci_parms.set_value = uci_malloc_string(value);
    /* Logic code end */

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}

uci_errno_t uci_commit(const uci_char *config)
{
    uci_parameter_t uci_parms;

    /* Parameter check. */
    UCI_PARMS_ASSERT(1); /* No parameter need to check. */

    uci_manager_memory(UCI_MEMORY_INIT, 0, 0);
    uci_initParameters(&uci_parms);

    uci_parms.command_type = UCI_COMMAND_COMMIT;
    if(config)
    {
        uci_parms.command_target = UCI_CMDLINE_CFG_FILE;
        uci_parms.config = uci_malloc_string(config);
    }
    else
        uci_parms.command_target = UCI_CMDLINE_PROCESS;

    uci_runCommand(&uci_parms);

    uci_parms.error_code = uci_getErrorCode();

    uci_freeParameters(&uci_parms);
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    uci_manager_memory(UCI_MEMORY_CHECK, 0, 0);

    return uci_parms.error_code;
}

