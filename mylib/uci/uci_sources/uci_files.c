/************************************************************************************************
 ** @File:    uci_files.c
 ** @Author:  Jan
 ** @Date:    2024/01/16
 ** @Brief:   TThis file provides the API for uci_files
 ************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "uci_files.h"
/* Public variables --------------------------------------------------------*/
/* Public macros -----------------------------------------------------------*/
/* Public typedef ----------------------------------------------------------*/
/* Public functions --------------------------------------------------------*/
/* Private variables -------------------------------------------------------*/
/* Private macros ----------------------------------------------------------*/
/* Private typedef ---------------------------------------------------------*/
/* Private functions -------------------------------------------------------*/
uci_file *uci_openFileStream(const uci_char *file_name, const uci_char *file_dir, uci_int pos, uci_bool write, uci_bool create)
{
	struct stat statbuf;
	uci_file *file = NULL;
    uci_char *file_path;
	uci_int fd, ret;
	uci_int mode = (write ? O_RDWR : O_RDONLY);

    if(!file_name || !file_dir)  UCI_DEBUG_ERROR(UCI_ERROR_ASSERT);

    file_path = uci_malloc_memory(strlen(file_dir)+strlen(file_name)+1);

    if(file_dir[strlen(file_dir)-1] == '/')
        sprintf(file_path, "%s%s", file_dir, file_name);
    else
        sprintf(file_path, "%s/%s", file_dir, file_name);

	if (create)
		mode |= O_CREAT;

	if (!write && ((stat(file_path, &statbuf) < 0) ||
		((statbuf.st_mode &  S_IFMT) != S_IFREG))) {
	    goto done;
	}

    LOG_DEBUG("open: %s", file_path);
#if UCI_FILE_LOCK
	fd = open(file_path, mode, UCI_FILEMODE);
	if (fd < 0)
		goto error;

	ret = flock(fd, (write ? LOCK_EX : LOCK_SH));
	if ((ret < 0) && (errno != ENOSYS))
		goto error;

	ret = lseek(fd, 0, pos);

	if (ret < 0)
		goto error;

	file = fdopen(fd, (write ? "w+" : "r"));
	if (file)
		goto done;
#else
    file = fopen(file_path, write?"w+":"r");
    if(file) goto done;
#endif

error:
	UCI_DEBUG_ERROR(UCI_ERROR_FILE_IO);
done:
    uci_free_memory(file_path);
	return file;
}

uci_void uci_closeFileStream(uci_file *stream)
{
	uci_int fd;

	if (!stream)
		return;

	fflush(stream);
	fd = fileno(stream);
	flock(fd, LOCK_UN);
	fclose(stream);
}
