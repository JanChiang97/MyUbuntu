/************************************************************************************************
 ** @File:    uci.h
 ** @Author:  Jan
 ** @Date:    2024/01/04
 ** @Brief:   This file include uci.c struct and manipulation functions.
 ************************************************************************************************/

#ifndef UCI_H
#define UCI_H
#ifdef __cplusplus ///<use C compiler                                           
extern "C"
{
#endif //__cplusplus
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "uci_types.h"
#include "uci_config.h"
#include "uci_function.h"
#include "uci_utils.h"
#include "uci_memory.h"
#include "uci_files.h"
#include "uci_error.h"

/* Exported variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/


uci_errno_t uci_add_config(const uci_char *config);
uci_errno_t uci_del_config(const uci_char *config);
uci_errno_t uci_set_section(const uci_char *config, const uci_char *section, const uci_char *type);
uci_errno_t uci_get_section(const uci_char *config, const uci_char *section, uci_char *buf, uci_uint32 size);
uci_errno_t uci_del_section(const uci_char *config, const uci_char *section);
uci_errno_t uci_rename_section(const uci_char *config, const uci_char *section, const uci_char *name);
uci_errno_t uci_set_option(const uci_char *config, const uci_char *section, const uci_char *option, const uci_char *value);
uci_errno_t uci_get_option(const uci_char *config, const uci_char *section, const uci_char *option, uci_char *buf, uci_uint32 size);
uci_errno_t uci_del_option(const uci_char *config, const uci_char *section, const uci_char *option);
uci_errno_t uci_rename_option(const uci_char *config, const uci_char *section, const uci_char *option, const uci_char *name);
uci_errno_t uci_add_list(const uci_char *config, const uci_char *section, const uci_char *list, const uci_char *value);
uci_errno_t uci_get_list(const uci_char *config, const uci_char *section, const uci_char *list, uci_char *buf, uci_uint32 size);
uci_errno_t uci_del_list(const uci_char *config, const uci_char *section, const uci_char *list, const uci_char *value);
uci_errno_t uci_del_list_all(const uci_char *config, const uci_char *section, const uci_char *list);
uci_errno_t uci_rename_list(const uci_char *config, const uci_char *section, const uci_char *list, const uci_char *name);
uci_errno_t uci_commit(const uci_char *config);

#ifdef __cplusplus ///<use C compiler                                           
}
#endif //__cplusplus
#endif

