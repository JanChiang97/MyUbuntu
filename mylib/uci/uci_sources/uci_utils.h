/************************************************************************************************
 ** @File:    uci_utils.h
 ** @Author:  Jan
 ** @Date:    2024/01/04
 ** @Brief:   This file include uci_utils.c struct and manipulation functions.
 ************************************************************************************************/

#ifndef UCI_UTILS_H
#define UCI_UTILS_H
#ifdef __cplusplus ///<use C compiler                                           
extern "C"
{
#endif //__cplusplus
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "uci.h"
/* Exported variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
#define LOG_PRINT(args...)  do{printf(args);printf("\r\n");}while(0)
#define UCI_OUTPUT(args...) do{printf(args);}while(0)

#if UCI_DEBUG_ENABLED
#define LOG_DEBUG(args...) do{printf("DEBUG_INFO:\t\t(%-26s:L%04d)\t\t", __FUNCTION__, __LINE__);printf(args);printf("\r\n");}while(0)
#else
#define LOG_DEBUG(args...) do{}while(0)
#endif



/* Exported functions --------------------------------------------------------*/
void uci_initParameters(uci_parameter_t *uci_parms);
void uci_freeParameters(uci_parameter_t *uci_parms);

uci_bool uci_parseLineSection(const uci_char *buf, uci_package_t *uci_package);
uci_bool uci_parseLineOption(const uci_char *buf, uci_section_t *uci_section);
uci_bool uci_parseLineList(const uci_char *buf, uci_section_t *uci_section);


uci_list_t    *uci_new_list(uci_void);
uci_package_t *uci_new_package(const uci_char *name);
uci_section_t *uci_new_section(const uci_char *name, uci_char *type);
uci_element_t *uci_new_element(const uci_char *name, uci_char *value);
uci_delta_t   *uci_new_delta(const uci_char *name, uci_char *value, uci_modify_cmd_t cmd);

uci_void uci_initDelta(uci_delta_t *delta);
uci_void uci_initElement(uci_element_t *element);
uci_void uci_initSection(uci_section_t *section);
uci_void uci_initPackage(uci_package_t *package);
uci_void uci_initList(uci_list_t *uci_list);


uci_void uci_insertSection(uci_section_t *uci_sec, uci_element_t *uci_elm);
uci_void uci_insertPackage(uci_package_t *uci_pkg, uci_section_t *uci_sec);
uci_void uci_insertList(uci_list_t *uci_list, uci_package_t *uci_pkg);
uci_void uci_addDelta(uci_void *father, uci_delta_t *new, uci_delta_type_t type);

uci_void uci_parseConfigDir(uci_list_t *uci_list);
uci_void uci_parseConfigFile(uci_list_t *uci_list, const uci_char *file_name);

uci_void uci_showDelta(uci_delta_t *delta);
uci_void uci_parseDeltaFile(uci_list_t *uci_list, const uci_char *file_name);
uci_void uci_parseDeltaDir(uci_list_t *uci_list);
uci_void uci_saveDeltaFile(uci_list_t *uci_list, const uci_char *file_name);

uci_void uci_mergeChanges(uci_list_t *uci_list);
uci_void uci_commitFiles(uci_list_t *uci_list);
uci_void uci_commitFile(uci_package_t *uci_package, const uci_char *dir);

uci_void uci_release_delta(uci_delta_t *delta);
uci_void uci_release_list(uci_list_t *uci_list);
uci_void uci_release_element(uci_element_t *uci_element);
uci_void uci_release_section(uci_section_t *uci_section);
uci_void uci_release_package(uci_package_t *uci_package);


#define UCI_LIST_INIT(list)              do{(list)->prev=(list); (list)->next=(list);}while(0)
#define UCI_LIST_REMOVE_NODE(node)        do{(node)->prev->next=(node)->next; (node)->next->prev=(node)->prev;}while(0)

#define UCI_LIST_FOREACH(list, ptr)      for((ptr)=(list)->next; (ptr)!=(list); (ptr)=(ptr)->next)
#define UCI_LIST_FOREACH_END(list, ptr)  ((ptr)==(list))
#define UCI_FATHER(son, father_type)    ((father_type)(son->father))
#ifdef __cplusplus ///<use C compiler                                           
}
#endif //__cplusplus
#endif

