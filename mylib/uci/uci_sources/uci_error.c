/************************************************************************************************
 ** @File:    uci_error.c
 ** @Author:  JanChiang
 ** @Date:    2024/01/26
 ** @Brief:   TThis file provides the API for uci_error
 ************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "uci_error.h"
/* Public variables --------------------------------------------------------*/
/* Public macros -----------------------------------------------------------*/
/* Public typedef ----------------------------------------------------------*/
/* Public functions --------------------------------------------------------*/
/* Private variables -------------------------------------------------------*/
/* Private macros ----------------------------------------------------------*/
/* Private typedef ---------------------------------------------------------*/
/* Private functions -------------------------------------------------------*/
static uci_errno_t uci_errno_code = UCI_ERROR_NONE;

uci_errno_t uci_getErrorCode(uci_void)
{
    return uci_errno_code;
}

uci_void uci_setErrorCode(uci_errno_t uci_errno)
{
    uci_errno_code = uci_errno;
}

uci_void uci_showErrorMessage(uci_errno_t uci_errno, const uci_char *func, uci_int line)
{
    uci_setErrorCode(uci_errno);

    switch ((uci_errno_t)uci_errno)
    {
    case UCI_ERROR_NONE:
        LOG_DEBUG("[errno = %d function:%s line:%d]: No error.", uci_errno, func, line);
        break;
    case UCI_ERROR_REQUREST:
        LOG_PRINT("[errno = %d function:%s line:%d]: Unknown request.", uci_errno, func, line);
        break;
    case UCI_ERROR_DIR_NOT_FOUND:
        LOG_PRINT("[errno = %d function:%s line:%d]: Directory does not exist.", uci_errno, func, line);
        break;
    case UCI_ERROR_CFG_NOT_FOUND:
        LOG_PRINT("[errno = %d function:%s line:%d]: Configuration not found.", uci_errno, func, line);
        break;
    case UCI_ERROR_CFG_FORMAT:
        LOG_PRINT("[errno = %d function:%s line:%d]: Invalid configuration.", uci_errno, func, line);
        break;
    case UCI_ERROR_SECTION_NOT_FOUND:
        LOG_PRINT("[errno = %d function:%s line:%d]: Section not found.", uci_errno, func, line);
        break;
    case UCI_ERROR_OPTION_NOT_FOUND:
        LOG_PRINT("[errno = %d function:%s line:%d]: Option not found.", uci_errno, func, line);
        break;
    case UCI_ERROR_FILE_IO:
        LOG_PRINT("[errno = %d function:%s line:%d]: File IO error.", uci_errno, func, line);
        break;
    case UCI_ERROR_ASSERT:
        LOG_PRINT("[errno = %d function:%s line:%d]: Parameters check.", uci_errno, func, line);
        break;
    case UCI_ERROR_MEMORY_ALLOC:
        LOG_PRINT("[errno = %d function:%s line:%d]: Failed to allocate memory.", uci_errno, func, line);
        break;
    case UCI_ERROR_FILE_PARSE:
        LOG_PRINT("[errno = %d function:%s line:%d]: Failed to parse config file.", uci_errno, func, line);
        break;
    case UCI_ERROR_UNKNOWN:
    case UCI_ERROR_MAX:
    default:
        LOG_PRINT("[errno = %d function:%s line:%d]: Unknown error.", uci_errno, func, line);
        break;
    }

#if EXIT_WHEN_ERROR
    /* If you compile uci as an executable program, simply exit and give an error code. */
    uci_manager_memory(UCI_MEMORY_RELEASE, 0, 0);
    exit(uci_errno);
#endif
}