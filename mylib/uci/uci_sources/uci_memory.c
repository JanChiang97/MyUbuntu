/************************************************************************************************
 ** @File:    uci_memory.c
 ** @Author:  Jan
 ** @Date:    2024/01/16
 ** @Brief:   TThis file provides the API for uci_memory
 ************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "uci_memory.h"
/* Public variables --------------------------------------------------------*/
static uci_memory *mem_tab = NULL; /* Manager all the heap memory. */
/* Public macros -----------------------------------------------------------*/
/* Public typedef ----------------------------------------------------------*/
/* Public functions --------------------------------------------------------*/
/* Private variables -------------------------------------------------------*/
/* Private macros ----------------------------------------------------------*/
/* Private typedef ---------------------------------------------------------*/
/* Private functions -------------------------------------------------------*/
uci_void uci_manager_memory(uci_int mem_cmd, uci_void *ptr, uci_int size)
{
    static uci_int mem_ptr_cnt = 1;     /* 指针计数 */
    static uci_int mem_ptr_len = 1;     /* 内存探针表长度 */

    uci_int index;

    if(!mem_tab && mem_cmd != UCI_MEMORY_INIT && mem_cmd != UCI_MEMORY_CHECK) return;

    switch (mem_cmd)
    {
    case UCI_MEMORY_INIT:
        if(!mem_tab)    {mem_tab = malloc(sizeof(uci_memory));mem_ptr_cnt=1;mem_ptr_len=1;}
        if(mem_tab)     mem_tab[0] = (uci_memory)mem_tab;
        break;
    case UCI_MEMORY_CHECK:
        if(mem_ptr_cnt)
            LOG_PRINT("%d pointers did not release", mem_ptr_cnt);
        break;
    case UCI_MEMORY_ADD:
        if(ptr) 
        {
            if(mem_ptr_len < ++mem_ptr_cnt)
            {
                mem_ptr_len++;
                mem_tab = realloc(mem_tab, sizeof(uci_void *)*mem_ptr_len);
                /* set new memory to 0 */
                mem_tab[mem_ptr_len-1] = (uci_memory)0;
            }

            if(mem_tab)
            {
                /* Record self */
                mem_tab[0] = (uci_memory)mem_tab;

                /* Search from the tail */  
                for(index = mem_ptr_len-1; index > 0; index--)
                {
                    if(mem_tab[index] == (uci_memory)0)
                    {
                        LOG_DEBUG("Add Memory(%d):%p", index, ptr);
                        mem_tab[index] = (uci_memory)ptr;
                        break;
                    }
                }
            }
        }
        break;
    case UCI_MEMORY_FREE:
        if(ptr)
        {
            for(index = 0; index < mem_ptr_len; index++)
            {
                if(mem_tab[index] == (uci_memory)ptr)
                {
                    LOG_DEBUG("Free Memory(%d):%p", index, ptr);
                    mem_tab[index] = (uci_memory)0;
                    mem_ptr_cnt--;
                    break;
                }
            }
        }
        break;
    case UCI_MEMORY_RELEASE:
        for(index = 1; index < mem_ptr_len; index++)
        {
            if(mem_tab[index] != (uci_memory)0)
            {
                LOG_DEBUG("Auto Free Memory(%d):%p", index, (uci_void*)mem_tab[index]);
                free((uci_void *)mem_tab[index]);
                mem_tab[index] = (uci_memory)0;
                mem_ptr_cnt--;
            }
        }
        
        if(mem_tab)
        {
            mem_tab[0] = (uci_memory)0;
            free(mem_tab); /* Free self */
            mem_tab = NULL;
            mem_ptr_cnt--;
        }
        break;
    default:
        break;
    }
}

uci_char *uci_malloc_string(const uci_char *src)
{
    uci_char *tar;

    if(!src) return NULL;

    tar = uci_malloc_memory(strlen(src)+2);
    memcpy(tar, src, strlen(src));

    return tar;
}

uci_void *uci_malloc_memory(uci_int size)
{
    uci_void *mem = malloc(sizeof(uci_char) * (size));
    if(!mem) UCI_DEBUG_ERROR(UCI_ERROR_MEMORY_ALLOC);

    uci_manager_memory(UCI_MEMORY_ADD, mem, size);
    memset(mem, 0, size);

    return mem;
}

uci_void *uci_realloc_memory(uci_void *memory, uci_int size)
{
    uci_void *mem;
    mem = realloc(memory, sizeof(uci_char) * (size));
    if(!mem) UCI_DEBUG_ERROR(UCI_ERROR_MEMORY_ALLOC);

    uci_manager_memory(UCI_MEMORY_FREE, memory, 0);
    uci_manager_memory(UCI_MEMORY_ADD, mem, size);

    return mem;
}

uci_void uci_free_memory(uci_void *memory)
{
    uci_manager_memory(UCI_MEMORY_FREE, memory, 0);
    if(memory) free(memory);
}