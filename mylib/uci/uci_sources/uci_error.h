/************************************************************************************************
 ** @File:    uci_error.h
 ** @Author:  Jan
 ** @Date:    2024/01/26
 ** @Brief:   This file include uci_error.c struct and manipulation functions.
 ************************************************************************************************/

#ifndef _UCI_ERROR_H_
#define _UCI_ERROR_H_
#ifdef __cplusplus ///<use C compiler                                           
extern "C"
{
#endif //__cplusplus
/* Includes ------------------------------------------------------------------*/
#include "uci.h"
/* Exported variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
uci_errno_t uci_getErrorCode(uci_void);
uci_void uci_setErrorCode(uci_errno_t uci_errno);
uci_void uci_showErrorMessage(uci_errno_t uci_errno, const uci_char *func, uci_int line);

/* What to do when a error found. */
#define UCI_ERROR_EXEC_RETURN_FALSE                 return false;
#define UCI_ERROR_EXEC_RETURN_VOID                  return;
#define UCI_ERROR_EXEC_RETURN_NULL                  return NULL;
#define UCI_ERROR_COPY_ERROR_CODE(error_code)       error_code = uci_getErrorCode();
#define UCI_ERROR_SET_ERROR_CODE(error_code)        uci_setErrorCode(error_code);

/* Show and set error code. */
#define UCI_DEBUG_ERROR(uci_errno)                  uci_showErrorMessage(uci_errno, __FUNCTION__, __LINE__)
/* Call this before the logic code to check paramters */
#define UCI_PARMS_ASSERT(expression)                if(!(expression)) UCI_DEBUG_ERROR(UCI_ERROR_ASSERT);
/* When we checked a error, we run @exec_code */
#define UCI_ERROR_ASSERT(exec_code)                 do{if(uci_getErrorCode()){exec_code}}while(0)

#ifdef __cplusplus ///<use C compiler                                           
}
#endif //__cplusplus
#endif

