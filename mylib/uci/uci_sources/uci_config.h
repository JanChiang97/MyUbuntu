/************************************************************************************************
 ** @File:    uci_config.h
 ** @Author:  Jan
 ** @Date:    2024/01/05
 ** @Brief:   This file include uci_config.c struct and manipulation functions.
 ************************************************************************************************/

#ifndef _UCI_CONFIG_H_
#define _UCI_CONFIG_H_
#ifdef __cplusplus ///<use C compiler                                           
extern "C"
{
#endif //__cplusplus
/* Includes ------------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
#define UCI_DEBUG_ENABLED                          0
#define UCI_FILE_LOCK                              0
#define UCI_DIRECT_ADD_SECTION_ENABLED             1

#define UCI_CONFDIR                 "/etc/uci"
#define UCI_SAVEDIR                 "/var/.uci"
#define UCI_DIRMODE                 0755
#define UCI_FILEMODE                0644

#define UCI_BUFFER_SIZE             1024
#define UCI_FIELD_LENGTH            64
#define UCI_CMD_DECRIPTION_LENGTH   64

#define UCI_SECTION_LINE_HEADER     "config"
#define UCI_OPTION_LINE_HEADER      "option"
#define UCI_LIST_LINE_HEADER        "list"

/* Exported typedef ----------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

#ifdef __cplusplus ///<use C compiler                                           
}
#endif //__cplusplus
#endif

