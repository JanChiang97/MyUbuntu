/************************************************************************************************
 ** @File:    uci_files.h
 ** @Author:  Jan
 ** @Date:    2024/01/16
 ** @Brief:   This file include uci_files.c struct and manipulation functions.
 ************************************************************************************************/

#ifndef _UCI_FILES_H_
#define _UCI_FILES_H_
#ifdef __cplusplus ///<use C compiler                                           
extern "C"
{
#endif //__cplusplus
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <errno.h>
#include <glob.h>

#include "uci_types.h"
#include "uci_memory.h"
#include "uci_utils.h"

/* Exported variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
uci_file *uci_openFileStream(const uci_char *file_name, const uci_char *file_dir, uci_int pos, uci_bool write, uci_bool create);
uci_void uci_closeFileStream(uci_file *stream);
#ifdef __cplusplus ///<use C compiler                                           
}
#endif //__cplusplus
#endif

