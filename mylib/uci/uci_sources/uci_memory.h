/************************************************************************************************
 ** @File:    uci_memory.h
 ** @Author:  Jan
 ** @Date:    2024/01/16
 ** @Brief:   This file include uci_memory.c struct and manipulation functions.
 ************************************************************************************************/

#ifndef _UCI_MEMORY_H_
#define _UCI_MEMORY_H_
#ifdef __cplusplus ///<use C compiler                                           
extern "C"
{
#endif //__cplusplus
/* Includes ------------------------------------------------------------------*/
#include "uci.h"
/* Exported variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
/* Heap memory manager. */
uci_void uci_manager_memory(uci_int mem_cmd, uci_void *ptr, uci_int size);
uci_char *uci_malloc_string(const uci_char *src);
uci_void *uci_malloc_memory(uci_int size);
uci_void *uci_realloc_memory(uci_void *memory, uci_int size);
uci_void uci_free_memory(uci_void *memory);

#ifdef __cplusplus ///<use C compiler                                           
}
#endif //__cplusplus
#endif

