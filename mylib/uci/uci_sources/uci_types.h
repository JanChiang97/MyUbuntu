/************************************************************************************************
 ** @File:    uci_types.h
 ** @Author:  Jan
 ** @Date:    2024/01/04
 ** @Brief:   This file include uci_types.c struct and manipulation functions.
 ************************************************************************************************/

#ifndef UCI_TYPES_H
#define UCI_TYPES_H
#ifdef __cplusplus ///<use C compiler                                           
extern "C"
{
#endif //__cplusplus
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Exported variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
typedef void            uci_void;
typedef char            uci_char;
typedef bool            uci_bool;
typedef int             uci_int;

typedef int8_t          uci_int8;
typedef int32_t         uci_int32;
typedef int64_t         uci_int64;

typedef uint8_t         uci_uint8;
typedef uint32_t        uci_uint32;
typedef uint64_t        uci_uint64;

typedef int64_t         uci_long;
typedef short int       uci_short;
typedef double          uci_double;

typedef FILE            uci_file;
typedef int32_t         uci_memory;

typedef enum
{
    UCI_MEMORY_INIT,
    UCI_MEMORY_CHECK,
    UCI_MEMORY_ADD,
    UCI_MEMORY_FREE,
    UCI_MEMORY_RELEASE,
    UCI_MEMORY_MAX,
} uci_memory_cmd_t;

typedef enum
{
    UCI_CMDLINE_PROCESS,
    UCI_CMDLINE_CFG_FILE,
    UCI_CMDLINE_SECTION,
    UCI_CMDLINE_OPTION,
    UCI_CMDLINE_VALUE,
    UCI_CMDLINE_MAX,
} uci_cmdline_t;

typedef enum
{
    UCI_MOD_CMD_NONE = 0,
    UCI_MOD_CMD_REMOVE,
    UCI_MOD_CMD_RENAME,
    UCI_MOD_CMD_ADD,
    UCI_MOD_CMD_ADD_LIST,
    UCI_MOD_CMD_REORDER,
    UCI_MOD_CMD_MAX,
} uci_modify_cmd_t;

static uci_char *uci_modify_cmd[UCI_MOD_CMD_MAX] =
{
    "none",
    "remove",
    "rename",
    "add",
    "add_list",
    "reorder",
};

typedef enum
{
    UCI_ERROR_NONE                  = 0,
    UCI_ERROR_REQUREST              = 1,
    UCI_ERROR_DIR_NOT_FOUND         = 2,
    UCI_ERROR_CFG_NOT_FOUND         = 3,
    UCI_ERROR_SECTION_NOT_FOUND     = 4,
    UCI_ERROR_OPTION_NOT_FOUND      = 5,
    UCI_ERROR_CFG_FORMAT            = 6,
    UCI_ERROR_FILE_IO               = 7,
    UCI_ERROR_ASSERT                = 8,
    UCI_ERROR_MEMORY_ALLOC          = 9,
    UCI_ERROR_FILE_PARSE            = 10,
    UCI_ERROR_UNKNOWN               = 11,
    UCI_ERROR_MAX,
} uci_errno_t;

typedef enum
{
    UCI_ELEMENT_NONE,
    UCI_ELEMENT_CONFIG,
    UCI_ELEMENT_SECTION,
    UCI_ELEMENT_OPTION,
    UCI_ELEMENT_LIST,
} uci_element_type_t;

typedef enum
{
    UCI_DELTA_LIST,
    UCI_DELTA_PACKAGE,
    UCI_DELTA_SECTION,
    UCI_DELTA_ELEMENT,
    UCI_DELTA_MAX,
} uci_delta_type_t;

typedef struct uci_delta
{
    uci_int         cmd;
    uci_int         type;
    uci_char        *name;
    uci_char        *value;
    /* 预想同一目标多次更改的情况发生，建立链表, 必须从尾部进行添加*/
    struct uci_delta *prev;
    struct uci_delta *next;
    void             *father;
} uci_delta_t;

typedef struct uci_element
{
    uci_bool        is_list;
    uci_delta_t     delta;
    uci_char        *name;
    uci_char        *value;
    struct uci_element   *prev;
    struct uci_element   *next;
    void            *father;
} uci_element_t;

typedef struct uci_section
{
    uci_element_t   element;
    uci_delta_t     delta;
    uci_char        *name;
    uci_char        *type;
    struct uci_section   *prev;
    struct uci_section   *next;
    void            *father;
} uci_section_t;

typedef struct uci_package
{
    uci_section_t   section;
    uci_delta_t     delta;
    uci_char        *name;
    struct uci_package *prev;
    struct uci_package *next;
    void            *father;
} uci_package_t;


typedef struct uci_list
{
    uci_package_t       package;
    uci_delta_t         delta;
    uci_char            *conf_path;
    uci_char            *save_path;
} uci_list_t;

typedef struct
{
    uci_int32   command_type;
    uci_int32   command_target;
    uci_errno_t error_code;
    uci_char    *config;
    uci_char    *section;
    uci_char    *option;
    uci_char    *set_value;
    uci_char    *ret_value;

    /* 为匿名节点预留 */
    uci_char    *section_type;
} uci_parameter_t;

/* Exported functions --------------------------------------------------------*/

#ifdef __cplusplus ///<use C compiler                                           
}
#endif //__cplusplus
#endif
