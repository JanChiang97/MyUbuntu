#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define autofree __attribute__((cleanup(free_stack)))

#define TEST_STRING	"Hello world!"

__attribute__ ((always_inline))
inline void free_stack(void *ptr) {
    free(*(void **) ptr);
}

char *auto_ptr;
char *normal_ptr;

void test_fun(const char *s)
{
	autofree char *auto_str = malloc(strlen(s)+1);
	memcpy(auto_str, s, strlen(s)+1);
	auto_ptr = auto_str;

	char *normal_str = malloc(strlen(s)+1);
	memcpy(normal_str, s, strlen(s)+1);
	normal_ptr = normal_str;

	printf("%s: auto_ptr address:%p\r\n", __FUNCTION__, (void*)auto_str);
	printf("%s: auto_ptr content:%s\r\n", __FUNCTION__, auto_ptr);
	printf("%s: normal_ptr address:%p\r\n", __FUNCTION__, (void*)normal_str);
	printf("%s: normal_ptr content:%s\r\n", __FUNCTION__, normal_ptr);
	
}

void main(void)
{
	test_fun(TEST_STRING);

	if(auto_ptr)
	{
		printf("%s: auto_ptr address:%p\r\n", __FUNCTION__, (void*)auto_ptr);
		printf("%s: auto_ptr content:%s\r\n", __FUNCTION__, auto_ptr);

		memcpy(auto_ptr, TEST_STRING, strlen(TEST_STRING)+1);
		printf("%s: auto_ptr content:%s\r\n", __FUNCTION__, auto_ptr);
	}

	
    if(normal_ptr)
    {
        printf("%s: normal_ptr address:%p\r\n", __FUNCTION__, (void*)normal_ptr);
        printf("%s: normal_ptr content:%s\r\n", __FUNCTION__, normal_ptr);
		free(normal_ptr);

		memcpy(normal_ptr, TEST_STRING, strlen(TEST_STRING)+1);
        printf("%s: normal_ptr content:%s\r\n", __FUNCTION__, normal_ptr);
    }
}
