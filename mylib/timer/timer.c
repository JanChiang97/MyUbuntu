#define _GNU_SOURCE

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#define THREAD_NUM      10

typedef unsigned long int uint64;

uint64 u64System_time;

#define OPEN_DEV_DBG                    1
#if (OPEN_DEV_DBG == 1)
#define DEV_DBG(format,...) printf("[ %ld.%03ld ] "format, \
                                  (u64System_time)/1000,(u64System_time)%1000, \
                                  ##__VA_ARGS__)
#else
#define DEV_DBG(format,...) do { } while (0)
#endif

pthread_mutex_t mutex_1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_2 = PTHREAD_MUTEX_INITIALIZER;

void *thread_rountine_1(void *args)
{
	struct timeval start_time;
	struct timeval system_time;
	gettimeofday(&start_time, NULL);
	uint64 start_time_ms = start_time.tv_sec * 1000 + start_time.tv_usec/1000;
	pthread_t selfid = pthread_self(); //
	DEV_DBG("thread_routine 1 : %ld \n", selfid);
    pthread_mutex_lock(&mutex_1);
	while(1){
		gettimeofday(&system_time, NULL);
		u64System_time = (system_time.tv_sec*1000 + system_time.tv_usec/1000) - start_time_ms;
		usleep(1000);
	}
    pthread_mutex_unlock(&mutex_1);
}

void *thread_rountine_2(void *args)
{
	pthread_t selfid = pthread_self(); //
	while(1)
	{
		DEV_DBG("thread_routine 2 : %ld \n", random()%1024);
    	pthread_mutex_lock(&mutex_2);
    	usleep(10000000);
    	pthread_mutex_unlock(&mutex_2);
	}
}


int main()
{
    pthread_t tid1, tid2;
    pthread_create(&tid1, NULL, thread_rountine_1, NULL);
    pthread_create(&tid2, NULL, thread_rountine_2, NULL);

/*
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
*/

	while(1){
		DEV_DBG("%s\n", __FUNCTION__);
		sleep(1);
	}
    return 0;
}

