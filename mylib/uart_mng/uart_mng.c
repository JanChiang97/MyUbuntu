#include "uart_mng.h"


static uart_interf_t uart_interf[UART_MAX] =
{
		{RS485_0, -1, "/dev/ttymxc1", "RS485_0", B115200},
		{RS485_1, -1, "/dev/ttymxc2", "RS485_1", B115200},
		{RS485_2, -1, "/dev/ttymxc3", "RS485_2", B115200},
		{RS232,   -1, "/dev/ttymxc5", "RS232",   B115200}
};

bool openUart(uart_sel_t uart)
{
	uart_interf[uart].fd = open(uart_interf[uart].file_path, O_RDWR | O_NOCTTY | O_SYNC  | O_NONBLOCK);
	if(uart_interf[uart].fd < 0)
	{
		return false;
	}
	return true;
}

int closeUart(uart_sel_t uart)
{
	if(uart_interf[uart].fd >= 0)
	{
		return close(uart_interf[uart].fd);
	}

	return 0;
}

int readUart(uart_sel_t uart, char *data, int size)
{
	return read(uart_interf[uart].fd, data, size);
}

int readUart_timeout( int fd, void *buf, int len, int timeout_ms )
{
    int ret;
    size_t rsum = 0;
    ret = 0;
    fd_set rset;
    struct timeval timeout;
    timeout.tv_sec = timeout_ms / 1000;
    timeout.tv_usec = ( timeout_ms - timeout.tv_sec * 1000 ) * 1000;
    FD_ZERO( &rset );
    FD_SET( fd, &rset );
    ret = select( fd + 1, &rset, NULL, NULL, &timeout );

    if ( ret == 0 ) {
        // timeout
        return -1;
    }

    do {
        ret = read( fd, ( char * )buf + rsum, len - rsum );
        if ( ret > 0 ) 
        {
            rsum += ret;
        }

    } while ( ret > 0 );

    return rsum?rsum:ret;
}

int writeUart(uart_sel_t uart, const char *data, int size)
{
	int len = write(uart_interf[uart].fd, data, size);
    if(len == size) tcdrain(uart_interf[uart].fd);    /* delay for output */
    return len;
}

bool setupUart(uart_sel_t uart)
{
    struct termios tty;

    if (tcgetattr(uart_interf[uart].fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return false;
    }

    cfsetospeed(&tty, (speed_t)uart_interf[uart].speed);
    cfsetispeed(&tty, (speed_t)uart_interf[uart].speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(uart_interf[uart].fd, TCSANOW, &tty) != 0)
    {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return false;
    }
    return true;
}

