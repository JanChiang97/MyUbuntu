#ifndef UART_MNG_H
#define UART_MNG_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

typedef struct
{
	uint8_t interf_id;
	int fd;
	char file_path[20];
	char inst_name[16];
	int speed;
}uart_interf_t;

typedef enum
{
	RS485_0,
	RS485_1,
	RS485_2,
	RS232,
	UART_MAX
}uart_sel_t;


bool openUart(uart_sel_t uart);
int closeUart(uart_sel_t uart);
int readUart(uart_sel_t uart, char *data, int size);
int writeUart(uart_sel_t uart, const char *data, int size);
bool setupUart(uart_sel_t uart);
#endif

