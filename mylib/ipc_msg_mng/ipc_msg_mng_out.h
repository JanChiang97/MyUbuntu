#ifndef IPC_MSG_MNG_OUT_H
#define IPC_MSG_MNG_OUT_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------*
 * INCLUDE FILES
 *-----------------------------------*/
#include <sys/ipc.h>
#include <sys/msg.h>
/*!
\defgroup PublicDefines Public Defines
\sgroup
*/
#define STRINGIFY_(val) #val
/* Converts a macro argument into a character constant.*/
#define STRINGIFY(val) STRINGIFY_(val)

/*-----------------------------------*
 * PUBLIC DEFINES
 *-----------------------------------*/
///This is a public define
#define DEBUG_MODE				1
#if DEBUG_MODE
#define LOG_PRINT(args...)	{printf(args); printf("\n");}
#else
#define LOG_PRINT(args...)
#endif // DEBUG_MODE


#define IPC_QUEUE_FILE_DIR		"/home/lab/ipc_kfile/"
#define IPC_SENDER_TYPE_LEN		sizeof(long)
#define IPC_MSG_PAYLOAD_LEN		256
#define IPC_MSG_SIZE			IPC_SENDER_TYPE_LEN + IPC_MSG_PAYLOAD_LEN


//Add queue at here
#define X_IPC_QUEUE_TYPE	\
	X(IPC_QUEUE_NONE)	\
	X(IPC_QUEUE_TEST)	\

//Add sender at here
/* If msgtyp is 0, then the first message in the queue is read. */
/* If msgtyp is greater than 0, then the first message in the queue of type msgtyp is read, unless MSG_EXCEPT was specified in msgflg, in which case the first message
         in the queue of type not equal to msgtyp will be read. */
#define X_IPC_SENDER_TYPE	\
	X(IPC_SENDER_TYPE_FIRST_ENTRY)	\
	X(IPC_SENDER_TYPE_TEST1)	\
	X(IPC_SENDER_TYPE_TEST2)	\

/*!\egroup*/
/*!
\defgroup PublicTypedef Public Typedefs
\sgroup
*/
/*-----------------------------------*
 * PUBLIC TYPEDEFS
 *-----------------------------------*/
///This is a public typedef
typedef enum
{
#define X(xidx) xidx,
	X_IPC_QUEUE_TYPE
#undef X
	IPC_QUEUE_MAX,
}ipc_queueType_t;

static char* ipc_queueTypeStringBuf[IPC_QUEUE_MAX] =
{
#define X(xidx) STRINGIFY(xidx),
	X_IPC_QUEUE_TYPE
#undef X
};

typedef enum
{
	/* If msgtyp is less than 0, then the first message in the queue with the lowest type less than or equal to the absolute value of msgtyp will be read. */
	IPC_SENDER_TYPE_LOW_PRIO = -1,
#define X(xidx) xidx,
	X_IPC_SENDER_TYPE
#undef X
	IPC_SENDER_TYPE_MAX,
}ipc_senderType_t;

static char* ipc_senderTypeStringBuf[IPC_SENDER_TYPE_MAX] =
{
#define X(xidx) STRINGIFY(xidx),
	X_IPC_SENDER_TYPE
#undef X
};

typedef struct
{
	union type
	{
		long	         type_long;
		ipc_senderType_t ipc_senderType;
	}type;

	char payload[IPC_MSG_PAYLOAD_LEN];
}ipc_msg_struct_t;

/*!\egroup*/
/*!
\defgroup PublicInline Public Inline Functions
\sgroup
*/
/*-----------------------------------*
 * PUBLIC INLINE FUNCTIONS
 *-----------------------------------*/
/***************************************************************************/
//   Function    :   Inline function name
//
//   Description:
/*! \brief Report a short description of this function
*/
//
//  Parameters and Returns:
/*!
\param param1: param1 meaning
\param param2: param2 meaning
\returns what function returns
*/
//  Notes:
/*!
Write here a more detailed description of this inline function
*/
/**************************************************************************/
//inline void publicInlineFunction(void param1, void param2 )
//{
//
//}


/*!\egroup*/


/*-----------------------------------*
 * PUBLIC VARIABLE DECLARATIONS
 *-----------------------------------*/

/*-----------------------------------*
 * PUBLIC FUNCTION PROTOTYPES
 *-----------------------------------*/
int ipcmsg_createMessageQueue(ipc_queueType_t msg_queueType);
int ipcmsg_destroyMessageQueue(int msg_queueId);
int ipcmsg_sendMessage(int msg_queueId, void *msg_p);
int ipcmsg_recvMessage(int msg_queueId, void *msg_p, ipc_senderType_t msg_senderType, int msg_flags);
long ipcmsg_createMessageAndSendToQueue(int msg_queueId, ipc_senderType_t msg_senderType, char *msg_payload, int msg_payloadSize);
long ipcmsg_recvFromQueueAndParseMessage(int msg_queueId, ipc_senderType_t msg_senderType, char *msg_payload, int msg_payloadSize, int msg_flags);
void ipcmsg_cleanQueueMessage(int msg_queueId, ipc_senderType_t msg_senderType);
#ifdef __cplusplus
}
#endif

#endif // IPC_MSG_MNG_OUT_H

/****************************************************************************
 ****************************************************************************/

