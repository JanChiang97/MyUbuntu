#---------------------------------------------------#
# Call the local compiler by using $LOCAL_A9_COMP_PATH. 
# Add the line "$LOCAL_A9_COMP_PATH" by using the command "gedit ~/.profile". 
# In "$LOCAL_A9_COMP_PATH" environment variable put the path for the A9 compiler.
# Ex. 1
# export $LOCAL_A9_COMP_PATH="/opt/fsl-imx-fb/5.4-zeus/environment-setup-cortexa9t2hf-neon-poky-linux-gnueabi"
# Ex. 2
# export $LOCAL_A9_COMP_PATH="/opt/poky/3.1.6/environment-setup-armv7at2hf-neon-poky-linux-gnueabi"
#---------------------------------------------------#
source $LOCAL_A9_COMP_PATH

export CFLAGS="$CFLAGS-pthread -lm"

make clean
make $1 
