/*! \mainpage ModuleName

\section intro Introduction
\brief A brief description of what this module does

Explain in detail how this module works.

\author  Name
 */

/*-----------------------------------*
 * INCLUDE FILES
 *-----------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "ipc_msg_mng.h"

/*!
\defgroup PublicVariables Public Variables
\sgroup
 */
/*-----------------------------------*
 * PUBLIC VARIABLE DEFINITIONS
 *-----------------------------------*/
///These variables can be used also by other SW modules

/*!\egroup*/
/*!
\defgroup PrivateVariables Private Variables
\sgroup
 */
/*-----------------------------------*
 * PRIVATE VARIABLE DEFINITIONS
 *-----------------------------------*/
///These variables can be used only by this module


/*!\egroup*/

/*!
\defgroup PublicFunctions Public Functions
@{
 */
/*==================================================================================================
                                       PUBLIC FUNCTIONS
==================================================================================================*/
/***************************************************************************/
//   Function    :   Public function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1   param1 meaning
\param param2   param2 meaning
\returns what function returns
 */
//  Notes: hexdump test.bin
/*!
Write here a more detailed description of this public function
 */
/**************************************************************************/
static key_t ipcmsg_getMessageQueueKey(char *file_path, int proj_id)
{
	FILE *file = NULL;
	FILE *pipe = NULL;
	DIR *ipc_dir = NULL;
	key_t key = -1;
	char cmd[64]={0};

	if(file_path == NULL) return key;

	if((ipc_dir = opendir(IPC_QUEUE_FILE_DIR)) == NULL)
	{
		int ret = mkdir(IPC_QUEUE_FILE_DIR, S_IRWXU | S_IRWXG | S_IRWXO);
		if(ret)
			return key;
	}
	else
		closedir(ipc_dir);


	//If file exist, get the key.
	file = fopen(file_path, "r");
	if(file)
	{
		fclose(file);
		key = ftok(file_path, proj_id);
		return key;
	}


	//If file not exist, create and set the attr. Return the key.
	file = fopen(file_path, "w");
	if(file)
	{
		fclose(file);
		sprintf(cmd, "chattr +i %s", file_path);
		pipe = popen(cmd, "r");
		if(pipe)
		{
			pclose(pipe);
			key = ftok(file_path, proj_id);
			return key;
		}
	}

	return key;
}

int ipcmsg_createMessageQueue(ipc_queueType_t msg_queueType)
{
	FILE *file;
	key_t key;
	char *kf_path;
	int kf_path_size;

	if(msg_queueType>=IPC_QUEUE_MAX || msg_queueType<=0) return -1;

	kf_path_size = strlen(IPC_QUEUE_FILE_DIR)+strlen(ipc_queueTypeStringBuf[msg_queueType])+1;
	kf_path = malloc(kf_path_size);

	if(kf_path != NULL)
	{
		memset(kf_path, 0, kf_path_size);
		sprintf(kf_path, "%s%s", IPC_QUEUE_FILE_DIR, ipc_queueTypeStringBuf[msg_queueType]);
		key = ipcmsg_getMessageQueueKey(kf_path, msg_queueType);
		LOG_PRINT("%s = [%d]", kf_path, key); 
		free(kf_path);

		if(key == -1) return -1;

		return msgget(key, 0666 | IPC_CREAT);
	}

	return -1;
}

/***************************************************************************/
//   Function    :   Public function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1   param1 meaning
\param param2   param2 meaning
\returns what function returns
 */
//  Notes: hexdump test.bin
/*!
Write here a more detailed description of this public function
 */
/**************************************************************************/
int ipcmsg_destroyMessageQueue(int msg_queueId)
{
	return msgctl(msg_queueId, IPC_RMID, NULL);
}

/***************************************************************************/
//   Function    :   Public function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1   param1 meaning
\param param2   param2 meaning
\returns what function returns
 */
//  Notes: hexdump test.bin
/*!
Write here a more detailed description of this public function
 */
/**************************************************************************/
int ipcmsg_sendMessage(int msg_queueId, void *msg_p)
{
	return msgsnd(msg_queueId, msg_p, IPC_MSG_PAYLOAD_LEN, IPC_NOWAIT);
}

/***************************************************************************/
//   Function    :   Public function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1   param1 meaning
\param param2   param2 meaning
\returns what function returns
 */
//  Notes: hexdump test.bin
/*!
Write here a more detailed description of this public function
 */
/**************************************************************************/
int ipcmsg_recvMessage(int msg_queueId, void *msg_p, ipc_senderType_t msg_senderType, int msg_flags)
{
	/*
	 * About msg_flags
	 * If no message of the requested type is available and IPC_NOWAIT isn't specified in msgflg, the calling process is blocked until one of the following  conditions  occurs:
	 * - A message of the desired type is placed in the queue.
	 * - The message queue is removed from the system.  In this case, the system call fails with errno set to EIDRM.
	 * - The  calling process catches a signal.  In this case, the system call fails with errno set to EINTR.
	 *   (msgrcv() is never automatically restarted after being interrupted by a signal handler, regardless of the setting of the SA_RESTART flag when establishing a signal handler.)
	 */
	return msgrcv(msg_queueId, msg_p, IPC_MSG_PAYLOAD_LEN, (long)msg_senderType, msg_flags);
}

/***************************************************************************/
//   Function    :   Public function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1   param1 meaning
\param param2   param2 meaning
\returns what function returns
 */
//  Notes: hexdump test.bin
/*!
Write here a more detailed description of this public function
 */
/**************************************************************************/
long ipcmsg_createMessageAndSendToQueue(int msg_queueId, ipc_senderType_t msg_senderType, char *msg_payload, int msg_payloadSize)
{
	int ret;
	ipc_msg_struct_t msg = {0};
	msg.type.ipc_senderType = msg_senderType;
	memcpy(msg.payload, msg_payload, msg_payloadSize>IPC_MSG_PAYLOAD_LEN?IPC_MSG_PAYLOAD_LEN:msg_payloadSize);

	ret = ipcmsg_sendMessage(msg_queueId, &msg);

	return ret>=0?msg.type.ipc_senderType:-1;
}

long ipcmsg_recvFromQueueAndParseMessage(int msg_queueId, ipc_senderType_t msg_senderType, char *msg_payload, int msg_payloadSize, int msg_flags)
{
	int ret;
	ipc_msg_struct_t msg = {0};
	ret = ipcmsg_recvMessage(msg_queueId, &msg, msg_senderType, msg_flags);
	memcpy(msg_payload, msg.payload, msg_payloadSize>IPC_MSG_PAYLOAD_LEN?IPC_MSG_PAYLOAD_LEN:msg_payloadSize);

	return ret>=0?msg.type.ipc_senderType:-1;
}

void ipcmsg_cleanQueueMessage(int msg_queueId, ipc_senderType_t msg_senderType)
{
	ipc_msg_struct_t msg = {0};

	while(ipcmsg_recvMessage(msg_queueId, &msg, msg_senderType, IPC_NOWAIT) >= 0);
}
/*!\}*/
/*!
\defgroup PrivateFunctions Private Functions
\sgroup
 */
/*==================================================================================================
                                       PRIVATE FUNCTIONS
==================================================================================================*/

/*!\egroup*/

/****************************************************************************
 ****************************************************************************/




