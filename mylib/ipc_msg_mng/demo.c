#include "ipc_msg_mng_out.h"
#include <stdio.h>

void main(void)
{
	long type;
	char tx[32] = "hello";
	char rx[32] = {0};

	int testQueueId = ipcmsg_createMessageQueue(IPC_QUEUE_TEST);
	LOG_PRINT("Queue Id[%d]", testQueueId);


	int n = 10;
	while(n--)
	{
		type = ipcmsg_createMessageAndSendToQueue(testQueueId, IPC_SENDER_TYPE_TEST1, tx, sizeof(tx));
		if(type == IPC_SENDER_TYPE_TEST1) LOG_PRINT("Sender[%s] send to queue[%s]:%s", ipc_senderTypeStringBuf[type], ipc_queueTypeStringBuf[IPC_QUEUE_TEST], tx);

		type = ipcmsg_createMessageAndSendToQueue(testQueueId, IPC_SENDER_TYPE_TEST2, tx, sizeof(tx));
		if(type == IPC_SENDER_TYPE_TEST2) LOG_PRINT("Sender[%s] send to queue[%s]:%s", ipc_senderTypeStringBuf[type], ipc_queueTypeStringBuf[IPC_QUEUE_TEST], tx);

	}

	//ipcmsg_cleanQueueMessage(testQueueId, IPC_SENDER_TYPE_TEST1);
	ipcmsg_cleanQueueMessage(testQueueId, IPC_SENDER_TYPE_TEST2);

	do
	{
		type = ipcmsg_recvFromQueueAndParseMessage(testQueueId, IPC_SENDER_TYPE_FIRST_ENTRY, rx, sizeof(tx), 0);
		if(type >= 0) LOG_PRINT("Queue[%s] recv from queue[%s]:%s", ipc_queueTypeStringBuf[IPC_QUEUE_TEST], ipc_senderTypeStringBuf[type], rx);
	} while(type >= 0);

}
