#ifndef IPC_MSG_MNG_H_
#define IPC_MSG_MNG_H_

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------*
 * INCLUDE FILES
 *-----------------------------------*/
#include "ipc_msg_mng_out.h"

/*!
\defgroup PrivateDefines Private Defines
\sgroup
 */
/*-----------------------------------*
 * PRIVATE DEFINES
 *-----------------------------------*/
///This is a private define


/*!\egroup*/
/*!
\defgroup PrivateTypedef Private Typedefs
\sgroup
 */
/*-----------------------------------*
 * PRIVATE TYPEDEFS
 *-----------------------------------*/
///This is a private typedef


/*!\egroup*/
/*!
\defgroup PrivateInline Private Inline Functions
\sgroup
 */
/*-----------------------------------*
 * PRIVATE INLINE FUNCTIONS
 *-----------------------------------*/
/***************************************************************************/
//   Function    :   Inline function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1 param1 meaning
\param param2 param2 meaning
\returns what function returns
 */
//  Notes:
/*!
Write here a more detailed description of this inline function
 */
/**************************************************************************/
//static inline void privateInlineFunction(void param1 ,void param2 )
//{
//
//}

/*-----------------------------------*
 * IMPORTED CALIBRATIONS
 *-----------------------------------*/
/* None */
/* Example:
extern const uint16_T TEMPLATECAL;
 */

/*!\egroup*/
/*-----------------------------------*
 * PRIVATE FUNCTION PROTOTYPES
 *-----------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // IPC_MSG_MNG_H_

/****************************************************************************
 ****************************************************************************/


