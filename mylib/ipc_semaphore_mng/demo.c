#include "ipc_semaphore_mng_out.h"
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

int semaphore;

void *thread_rountine_1(void *args)
{
    LOG_PRINT("%s Created.", __FUNCTION__);

    while(1)
    {
        ipc_semaphoreMutexLock(semaphore);
        LOG_PRINT("%s Exec.", __FUNCTION__);
        sleep(1);
        ipc_semaphoreMutexUnlock(semaphore);
        usleep(1000);
    }
}

void *thread_rountine_2(void *args)
{
    LOG_PRINT("%s Created.", __FUNCTION__);

    while (1)
    {
        ipc_semaphoreMutexLock(semaphore);
        LOG_PRINT("%s Exec.", __FUNCTION__);
        sleep(5);
        ipc_semaphoreMutexUnlock(semaphore);
        usleep(1000);
    }
}


int main(int argc, char const *argv[])
{
    /* code */
    pthread_t tid1, tid2;

    semaphore = ipc_semaphoreMutexInit(IPC_SEMAPHORE_TEST);
    LOG_PRINT("Semaphore Id = [%d].", semaphore);

    pthread_create(&tid1, NULL, thread_rountine_1, NULL);
    pthread_create(&tid2, NULL, thread_rountine_2, NULL);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    return 0;
}
