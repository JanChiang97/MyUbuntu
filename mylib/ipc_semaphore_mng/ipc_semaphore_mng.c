/*! \mainpage ModuleName

\section intro Introduction
\brief A brief description of what this module does

Explain in detail how this module works.

\author  Name
 */

/*-----------------------------------*
 * INCLUDE FILES
 *-----------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "ipc_semaphore_mng.h"

/*!
\defgroup PublicVariables Public Variables
\sgroup
 */
/*-----------------------------------*
 * PUBLIC VARIABLE DEFINITIONS
 *-----------------------------------*/
///These variables can be used also by other SW modules

/*!\egroup*/
/*!
\defgroup PrivateVariables Private Variables
\sgroup
 */
/*-----------------------------------*
 * PRIVATE VARIABLE DEFINITIONS
 *-----------------------------------*/
///These variables can be used only by this module


/*!\egroup*/

/*!
\defgroup PublicFunctions Public Functions
@{
 */
/*==================================================================================================
                                       PUBLIC FUNCTIONS
==================================================================================================*/
/***************************************************************************/
//   Function    :   Public function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1   param1 meaning
\param param2   param2 meaning
\returns what function returns
 */
//  Notes: hexdump test.bin
/*!
Write here a more detailed description of this public function
 */
/**************************************************************************/
static key_t ipcsem_getSemaphoreKey(char *file_path, int proj_id)
{
	FILE *file = NULL;
	FILE *pipe = NULL;
	DIR *ipc_dir = NULL;
	key_t key = -1;
	char cmd[64]={0};

	if(file_path == NULL) return key;

	if((ipc_dir = opendir(IPC_SEMAPHORE_KFILE_DIR)) == NULL)
	{
		int ret = mkdir(IPC_SEMAPHORE_KFILE_DIR, S_IRWXU | S_IRWXG | S_IRWXO);
		if(ret)
			return key;
	}
	else
		closedir(ipc_dir);

	//If file exist, get the key.
	file = fopen(file_path, "r");
	if(file)
	{
		fclose(file);
		key = ftok(file_path, proj_id);
		return key;
	}

	//If file not exist, create and set the attr. Return the key.
	file = fopen(file_path, "w");
	if(file)
	{
		fclose(file);
		sprintf(cmd, "chattr +i %s", file_path);
		pipe = popen(cmd, "r");
		if(pipe)
		{
			pclose(pipe);
			key = ftok(file_path, proj_id);
			return key;
		}
	}

	return key;
}

int ipcsem_createSemaphore(ipc_semaphoreType_t ipc_semaphoreType, int size)
{
	key_t key;
	int ipc_semId = -1;
	int kf_path_size;
	char *kf_path = NULL;

	if(ipc_semaphoreType >= IPC_SEMAPHORE_MAX || ipc_semaphoreType <=0 ) return ipc_semId;

	kf_path_size = strlen(IPC_SEMAPHORE_KFILE_DIR) + strlen(ipc_semaphore_type_string[ipc_semaphoreType])+1;
	kf_path = malloc(kf_path_size);

	if(kf_path != NULL)
	{
		memset(kf_path, 0, kf_path_size);
		sprintf(kf_path, "%s%s", IPC_SEMAPHORE_KFILE_DIR, ipc_semaphore_type_string[ipc_semaphoreType]);
		key = ipcsem_getSemaphoreKey(kf_path, ipc_semaphoreType);
		LOG_PRINT("%s = [%d]", kf_path, key);
		free(kf_path);

		if(key == -1) return ipc_semId;

		ipc_semId = semget(key, 1, 0666|IPC_CREAT|IPC_EXCL);
		if(ipc_semId != -1)
		{
			union semun sem_union;
			sem_union.val = size;	//defalut
			if(semctl( ipc_semId, 0, SETVAL, sem_union ))
			{
				semctl(ipc_semId, 0, IPC_RMID);
				ipc_semId = -1;
			}
		}
		else
		{
			ipc_semId = semget(key, 1, 0666 | IPC_CREAT);
		}
	}

	return ipc_semId;
}

/***************************************************************************/
//   Function    :   Public function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1   param1 meaning
\param param2   param2 meaning
\returns what function returns
 */
//  Notes: hexdump test.bin
/*!
Write here a more detailed description of this public function
 */
/**************************************************************************/
int ipcsem_destroySemaphore(int ipc_semphoreId)
{
	return semctl(ipc_semphoreId, 0, IPC_RMID);
}

/***************************************************************************/
//   Function    :   Public function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1   param1 meaning
\param param2   param2 meaning
\returns what function returns
 */
//  Notes: hexdump test.bin
/*!
Write here a more detailed description of this public function
 */
/**************************************************************************/
// P操作:
//  若信号量值为1，获取资源并将信号量值-1
//  若信号量值为0，进程挂起等待
int ipcsem_pgetSemaphore(int ipc_semphoreId)
{
	struct sembuf sem;
	sem.sem_num = 0;  					//信号量编号  对第一个信号量操作
	sem.sem_op = -1;  					//V操作，加一
	sem.sem_flg = SEM_UNDO;
    return semop(ipc_semphoreId,&sem,1); 	//参数2为结构体指针
}

/***************************************************************************/
//   Function    :   Public function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1   param1 meaning
\param param2   param2 meaning
\returns what function returns
 */
//  Notes: hexdump test.bin
/*!
Write here a more detailed description of this public function
 */
/**************************************************************************/
// V操作：
//  释放资源并将信号量值+1
//  如果有进程正在挂起等待，则唤醒它们
int ipcsem_vbackSemaphore(int ipc_semphoreId)
{
	struct sembuf sem;
	sem.sem_num = 0;  					//信号量编号  对第一个信号量操作
	sem.sem_op = 1;  					//V操作，加一
	sem.sem_flg = SEM_UNDO;
    return semop(ipc_semphoreId,&sem,1); 	//参数2为结构体指针
}

/*!\}*/
/*!
\defgroup PrivateFunctions Private Functions
\sgroup
 */
/*==================================================================================================
                                       PRIVATE FUNCTIONS
==================================================================================================*/

/*!\egroup*/

/****************************************************************************
 ****************************************************************************/




