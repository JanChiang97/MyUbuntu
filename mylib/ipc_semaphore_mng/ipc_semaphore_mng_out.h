#ifndef IPC_SEM_MNG_OUT_H
#define IPC_SEM_MNG_OUT_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------*
 * INCLUDE FILES
 *-----------------------------------*/
#include <sys/ipc.h>
#include <sys/sem.h>
/*!
\defgroup PublicDefines Public Defines
\sgroup
*/
/*-----------------------------------*
 * PUBLIC DEFINES
 *-----------------------------------*/
///This is a public define
#define STRINGIFY_(val) #val                                                    
/* Converts a macro argument into a character constant.*/                       
#define STRINGIFY(val) STRINGIFY_(val) 
/*!\egroup*/
/*!
\defgroup PublicTypedef Public Typedefs
\sgroup
*/
/*-----------------------------------*
 * PUBLIC TYPEDEFS
 *-----------------------------------*/
///This is a public typedef
#define DEBUG_MODE				1
#if DEBUG_MODE
#define LOG_PRINT(args...)	{printf(args); printf("\n");}
#else
#define LOG_PRINT(args...)
#endif // DEBUG_MODE


#define IPC_SEMAPHORE_KFILE_DIR				"/home/lab/ipc_kfile/"

#define X_SEMAPHORE_TYPE		\
	X(IPC_SEMAPHORE_NONE)		\
	X(IPC_SEMAPHORE_TEST)		\


typedef enum
{
#define X(xidx) xidx,
	X_SEMAPHORE_TYPE
#undef X
	IPC_SEMAPHORE_MAX,
}ipc_semaphoreType_t;

static char* ipc_semaphore_type_string[IPC_SEMAPHORE_MAX] =
{
#define X(xidx) STRINGIFY(xidx),
	X_SEMAPHORE_TYPE
#undef X
};

/*!\egroup*/
/*!
\defgroup PublicInline Public Inline Functions
\sgroup
*/
/*-----------------------------------*
 * PUBLIC INLINE FUNCTIONS
 *-----------------------------------*/
/***************************************************************************/
//   Function    :   Inline function name
//
//   Description:
/*! \brief Report a short description of this function
*/
//
//  Parameters and Returns:
/*!
\param param1: param1 meaning
\param param2: param2 meaning
\returns what function returns
*/
//  Notes:
/*!
Write here a more detailed description of this inline function
*/
/**************************************************************************/
//inline void publicInlineFunction(void param1, void param2 )
//{
//
//}


/*!\egroup*/


/*-----------------------------------*
 * PUBLIC VARIABLE DECLARATIONS
 *-----------------------------------*/

/*-----------------------------------*
 * PUBLIC FUNCTION PROTOTYPES
 *-----------------------------------*/
int ipcsem_createSemaphore(ipc_semaphoreType_t ipc_semaphoreType, int size);
int ipcsem_destroySemaphore(int ipc_semphoreId);
int ipcsem_pgetSemaphore(int ipc_semphoreId);
int ipcsem_vbackSemaphore(int ipc_semphoreId);

#define ipc_semaphoreMutexInit(ipc_semaphoreType)	ipcsem_createSemaphore(ipc_semaphoreType, 1)
#define ipc_semaphoreMutexLock(ipc_semphoreId)		ipcsem_pgetSemaphore(ipc_semphoreId)
#define ipc_semaphoreMutexUnlock(ipc_semphoreId)	ipcsem_vbackSemaphore(ipc_semphoreId);
#ifdef __cplusplus
}
#endif

#endif // IPC_MSG_MNG_OUT_H

/****************************************************************************
 ****************************************************************************/

