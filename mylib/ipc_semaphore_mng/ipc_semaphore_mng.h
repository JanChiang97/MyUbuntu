#ifndef IPC_SEM_MNG_H_
#define IPC_SEM_MNG_H_

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------*
 * INCLUDE FILES
 *-----------------------------------*/
#include "ipc_semaphore_mng_out.h"

/*!
\defgroup PrivateDefines Private Defines
\sgroup
 */
/*-----------------------------------*
 * PRIVATE DEFINES
 *-----------------------------------*/
///This is a private define

/*!\egroup*/
/*!
\defgroup PrivateTypedef Private Typedefs
\sgroup
 */
/*-----------------------------------*
 * PRIVATE TYPEDEFS
 *-----------------------------------*/
///This is a private typedef
union semun {
   int              val;    /* Value for SETVAL */
   struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
   unsigned short  *array;  /* Array for GETALL, SETALL */
   struct seminfo  *__buf;  /* Buffer for IPC_INFO
                               (Linux-specific) */
};

/*!\egroup*/
/*!
\defgroup PrivateInline Private Inline Functions
\sgroup
 */
/*-----------------------------------*
 * PRIVATE INLINE FUNCTIONS
 *-----------------------------------*/
/***************************************************************************/
//   Function    :   Inline function name
//
//   Description:
/*! \brief Report a short description of this function
 */
//
//  Parameters and Returns:
/*!
\param param1 param1 meaning
\param param2 param2 meaning
\returns what function returns
 */
//  Notes:
/*!
Write here a more detailed description of this inline function
 */
/**************************************************************************/
//static inline void privateInlineFunction(void param1 ,void param2 )
//{
//
//}

/*-----------------------------------*
 * IMPORTED CALIBRATIONS
 *-----------------------------------*/
/* None */
/* Example:
extern const uint16_T TEMPLATECAL;
 */

/*!\egroup*/
/*-----------------------------------*
 * PRIVATE FUNCTION PROTOTYPES
 *-----------------------------------*/

#ifdef __cplusplus
}
#endif

#endif // IPC_MSG_MNG_H_

/****************************************************************************
 ****************************************************************************/


